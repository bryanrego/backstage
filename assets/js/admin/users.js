import Vue from 'vue';
import InheritanceList from './inheritance-list';

new Vue({
    el: '#vue-inheritances',
    components: {
        InheritanceList
    }
});
