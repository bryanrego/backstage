import Vue from 'vue';
import GroupsTable from './groups-table';

new Vue({
    el: '#vue-groups',
    components: {
        GroupsTable
    }
});
