const $ = require('jquery');
global.$ = global.jQuery = $;

const bootstrap = window.bootstrap = require('bootstrap');
require('bootstrap-select/js/bootstrap-select');

require('../css/app.scss');

const imagesContext = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

window.addEventListener('DOMContentLoaded', () => {
    // enable footer popover
    new bootstrap.Popover('#footerHelp', {
        html: true,
        placement: 'top',
        trigger: 'focus'
    });
})
