import Vue from 'vue';
import GuestlistTable from './guestlist-table';

new Vue({
    el: '#vue-guestlist',
    components: {
        GuestlistTable
    }
});
