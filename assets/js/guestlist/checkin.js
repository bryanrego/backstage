import Vue from 'vue';
import CheckinTable from './checkin-table';

new Vue({
    el: '#vue-checkin',
    components: {
        CheckinTable
    }
});
