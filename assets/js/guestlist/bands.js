import Vue from 'vue';
import BandsTable from './bands-table';

new Vue({
    el: '#vue-bands',
    components: {
        BandsTable
    }
});
