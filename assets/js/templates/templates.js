$(function () {
    $('#variables-add').click(function (e) {
        const list = $('#variables-list');
        let counter = list.data('widget-counter');

        // grab the prototype and clone it
        const newWidget = $('#variables-prototype').children().first().clone();

        const input = newWidget.children().first().children().first();
        // replace __name__
        input.attr('id', input.attr('id').replace(/__name__/g, counter));
        input.attr('name', input.attr('name').replace(/__name__/g, counter));

        // Increase the counter
        counter++;
        // And store it, the length cannot be used if deleting widgets is allowed
        list.data('widget-counter', counter);

        // create a new list element and add it to the list
        list.children().last().before(newWidget);
        input.focus();
    });

    $('#variables-list').on('click', '.variables-copy', function (e) {
        let input = $(this).parent().prev().val();
        if (!input)
            return;
        input = ' %' + input + '% ';

        const textarea = $('#template_form_text');
        const caret = textarea[0].selectionStart;
        const text = textarea.val();
        const before = text.substring(0, caret);
        const after = text.substring(caret);
        // trim space at start if before ends with space or newline
        if (!before || /\s$/.test(before))
            input = input.trimStart();
        // trim space at end if after starts with space
        if (after.startsWith(' '))
            input = input.trimEnd();
        textarea.val(before + input + after);
        textarea.focus();
        textarea.caretTo(caret + input.length - 1)
    });

    $.caretTo = function (el, index) {
        if (el.createTextRange) {
            const range = el.createTextRange();
            range.move("character", index);
            range.select();
        } else if (el.selectionStart != null) {
            el.focus();
            el.setSelectionRange(index, index);
        }
    };

    $.fn.caretTo = function (index, offset) {
        return this.queue(function (next) {
            if (isNaN(index)) {
                let i = $(this).val().indexOf(index);

                if (offset === true) {
                    i += index.length;
                } else if (offset) {
                    i += offset;
                }

                $.caretTo(this, i);
            } else {
                $.caretTo(this, index);
            }

            next();
        });
    };
});
