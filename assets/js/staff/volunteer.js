import {del, post} from '../api';

$(() => {
    $('input[name="volunteer-group"]').on('change', e => {
        const checkbox = e.target;
        checkbox.setAttribute('disabled', true);

        if (checkbox.checked) {
            post('volunteergroup', {
                volunteer: parseInt(checkbox.dataset.volunteer),
                group: parseInt(checkbox.dataset.group)
            }).then(() => {
                checkbox.removeAttribute('disabled');
            }).catch(error => {
                console.error(error);
                checkbox.removeAttribute('disabled');
            })
        } else {
            del('volunteergroup', {
                volunteer: parseInt(checkbox.dataset.volunteer),
                group: parseInt(checkbox.dataset.group)
            }).then(() => {
                checkbox.removeAttribute('disabled');
            }).catch(error => {
                console.error(error);
                checkbox.removeAttribute('disabled');
            })
        }
    })
});
