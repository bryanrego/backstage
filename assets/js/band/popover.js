const bootstrap = require("bootstrap");

window.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('[data-bs-toggle="popover"]').forEach(element => {
        new bootstrap.Popover(element, {
            html: true,
            trigger: 'focus'
        });
    });
});
