import Vue from 'vue';
import ItemScanner from './item-scanner';

new Vue({
   el: '#vue-scanner',
   components: {
       ItemScanner
   }
});
