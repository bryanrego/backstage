window.addEventListener('DOMContentLoaded', () => {
    // set form values on modal open
    const form = document.querySelector('form[name="edit_form"]');
    const nameInput = document.getElementById('edit_form_name');
    document.getElementById('modal-edit').addEventListener('show.bs.modal', e => {
        const button = e.relatedTarget;
        form.setAttribute('action', button.dataset.action);
        nameInput.value = button.dataset.category;
    });
});
