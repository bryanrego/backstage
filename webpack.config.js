const Encore = require('@symfony/webpack-encore');
const path = require('path');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('app_sidebar', './assets/js/app_sidebar.js')

    .addEntry('app', './assets/js/app.js')
    .addEntry('admin/groups', './assets/js/admin/groups.js')
    .addEntry('admin/users', './assets/js/admin/users.js')
    .addEntry('band/application', './assets/js/band/application.js')
    .addEntry('band/applications', './assets/js/band/applications.js')
    .addEntry('band/finals', './assets/js/band/finals.js')
    .addEntry('band/finals_control', './assets/js/band/finals_control.js')
    .addEntry('band/popover', './assets/js/band/popover.js')
    .addEntry('band/social_media', './assets/js/band/social_media.js')
    .addEntry('band/spotify', './assets/js/band/spotify.js')
    .addEntry('band/ytplayer', './assets/js/band/ytplayer.js')
    .addEntry('guestlist/bands', './assets/js/guestlist/bands.js')
    .addEntry('guestlist/checkin', './assets/js/guestlist/checkin.js')
    .addEntry('guestlist/guestlist', './assets/js/guestlist/guestlist.js')
    .addEntry('inventory/categories', './assets/js/inventory/categories.js')
    .addEntry('inventory/item', './assets/js/inventory/item.js')
    .addEntry('inventory/scan', './assets/js/inventory/scan.js')
    .addEntry('shift/self', './assets/js/shift/self.js')
    .addEntry('shift/shift', './assets/js/shift/shift.js')
    .addEntry('staff/staff', './assets/js/staff/staff.js')
    .addEntry('staff/volunteer', './assets/js/staff/volunteer.js')
    .addEntry('templates/templates', './assets/js/templates/templates.js')
    .addEntry('volunteer', './assets/js/volunteer.js')

    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    .enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.targets = 'defaults'
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // enables Sass/SCSS support
    .enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    //.enableReactPreset()

    // uncomment to enable Vue.js
    .enableVueLoader(() => {}, {
        runtimeCompilerBuild: true
    })

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    .enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()

    // enable HMR with dev-server. See https://symfony.com/doc/current/frontend/encore/dev-server.html
    .configureDevServerOptions(options => {
        options.https = {
            pfx: path.join(process.env.HOME, '.symfony/certs/default.p12')
        };
        options.hot = true;
        options.liveReload = true;
        options.watchFiles = [
            './assets/**/*',
            './templates/**/*',
        ];
    })
;

module.exports = Encore.getWebpackConfig();
