<?php

namespace App\Volunteer\Controller;

use App\AbstractApiController;
use App\Security\Entity\User;
use App\System\Application\MailService;
use App\System\Entity\EmailRecord;
use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftVolunteer;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Entity\VolunteerGroup;
use App\Volunteer\Repository\ShiftVolunteerRepository;
use App\Volunteer\Repository\VolunteerRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

// TODO: move all routes to dedicated controllers

/**
 * @deprecated Use dedicated controllers to model api routes
 */
class ApiController extends AbstractApiController {

    // VOLUNTEER

    #[Route(path: '/api/volunteergroup', methods: ['POST'])]
    public function postVolunteerGroup(Request $request, EntityManagerInterface $doctrine) {
        if (!$this->isGranted('api.volunteergroup'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'volunteer' => 'int',
            'group' => 'int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var Volunteer $volunteer */
        $volunteer = $doctrine->find('\App\Volunteer\Entity\Volunteer', $data->volunteer);
        if ($volunteer === null)
            return $this->responseBadRequest();
        /** @var VolunteerGroup $group */
        $group = $doctrine->find('\App\Volunteer\Entity\VolunteerGroup', $data->group);
        if ($group === null)
            return $this->responseBadRequest();

        $volunteer->addGroup($group);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/api/volunteergroup', methods: ['DELETE'])]
    public function delVolunteerGroup(Request $request, EntityManagerInterface $doctrine) {
        if (!$this->isGranted('api.volunteergroup'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'volunteer' => 'int',
            'group' => 'int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var Volunteer $volunteer */
        $volunteer = $doctrine->find('\App\Volunteer\Entity\Volunteer', $data->volunteer);
        if ($volunteer === null)
            return $this->responseBadRequest();
        /** @var VolunteerGroup $group */
        $group = $doctrine->find('\App\Volunteer\Entity\VolunteerGroup', $data->group);
        if ($group === null)
            return $this->responseBadRequest();

        $volunteer->removeGroup($group);
        $doctrine->flush();

        return $this->responseOk();
    }

    /**
     * @Route("/api/shiftvolunteer/{id<\d+>}")
     */
    public function getShiftVolunteer($id, EntityManagerInterface $doctrine) {
        /** @var Shift $shift */
        $shift = $doctrine->find('\App\Volunteer\Entity\Shift', $id);
        if ($shift === null)
            return $this->responseBadRequest();

        if (!$this->isGranted('api.shiftvolunteer', $shift))
            return $this->responseForbidden();

        if ($shift->getVolunteers()->isEmpty())
            return $this->responseOk();
        $volunteer = $shift->getVolunteers()[0]->getVolunteer();
        return $this->responseOk([
            'volunteerId' => $volunteer->getId(),
            'volunteerName' => $volunteer->getFirstname() . ' ' . $volunteer->getSurname(),
            'shifts' => $volunteer->getOtherShifts($shift),
            'conflict' => $volunteer->hasConflictingShift($shift),
        ]);
    }

    #[Route(path: '/api/shiftvolunteer', methods: ['POST'])]
    public function postShiftVolunteer(Request $request, EntityManagerInterface $doctrine) {
        $data = $this->resolveBody($request, [
            'shift' => 'int',
            'volunteer' => '?int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var Shift $shift */
        $shift = $doctrine->find('\App\Volunteer\Entity\Shift', $data->shift);
        if ($shift === null)
            return $this->responseBadRequest();

        if (!$this->isGranted('api.shiftvolunteer', $shift))
            return $this->responseForbidden();

        if ($data->volunteer != null) {
            /** @var Volunteer $volunteer */
            $volunteer = $doctrine->find('\App\Volunteer\Entity\Volunteer', $data->volunteer);
            if ($volunteer === null)
                return $this->responseBadRequest();
            if (!$shift->getVolunteers()->isEmpty()) {
                $sv = $shift->getVolunteers()[0];
                $sv->setConfirmed(false);
                $sv->setVolunteer($volunteer);
            } else {
                $sv = new ShiftVolunteer();
                $volunteer->addShift($sv);
                $shift->setVolunteer($sv);
                $doctrine->persist($sv);
            }
            $doctrine->flush();

            return $this->responseOk([
                'volunteerId' => $volunteer->getId(),
                'volunteerName' => $volunteer->getFirstname() . ' ' . $volunteer->getSurname(),
                'shifts' => $volunteer->getOtherShifts($shift),
                'conflict' => $volunteer->hasConflictingShift($shift)
            ]);
        } else {
            $shift->removeVolunteers();
            $doctrine->flush();
            return $this->responseOk();
        }
    }

    #[Route(path: '/api/confirmshift', methods: ['POST'])]
    public function postConfirmShift(Request $request, EntityManagerInterface $doctrine) {
        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'shift' => 'int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var Shift $shift */
        $shift = $doctrine->find('\App\Volunteer\Entity\Shift', $data->shift);
        if ($shift == null)
            return $this->responseBadRequest();

        /** @var User $user */
        $user = $this->getUser();
        $vol = $user->getVolunteer();
        if ($vol == null)
            return $this->responseForbidden();

        /** @var ShiftVolunteerRepository $repo */
        $repo = $doctrine->getRepository('\App\Volunteer\Entity\ShiftVolunteer');
        /** @var ShiftVolunteer[] $sv */
        $sv = $repo->findBy([
            'shift' => $shift,
            'volunteer' => $vol->getId()
        ]);
        if (empty($sv))
            return $this->responseBadRequest();
        $sv[0]->setConfirmed(true);
        $doctrine->flush();

        return $this->responseOk();
    }

    // BAND CHECKIN

    #[Route(path: '/api/bandmemberarrived', methods: ['POST'])]
    public function postBandMemberArrived(Request $request, EntityManagerInterface $doctrine) {
        if (!$this->isGranted('checkin.info'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'member' => 'int',
            'arrived' => 'bool'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $member = $doctrine->find('\App\Band\Entity\BandMember', $data->member);
        if ($member == null)
            return $this->responseBadRequest();
        $member->setArrived($data->arrived);
        $doctrine->flush();

        return $this->responseOk([
            'all' => $member->getTravelParty()->allArrived(),
            'tp' => $member->getTravelParty()->getId()
        ]);
    }
}
