<?php

namespace App\Volunteer\Enum;

/**
 * Shift area can either describe shifts for construction, during the festival, or for dismantling
 */
enum ShiftAreaType: string {
    case CONSTRUCTION = 'construction';
    case FESTIVAL = 'festival';
    case DISMANTLING = 'dismantling';
}
