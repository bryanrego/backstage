<?php

namespace App\Volunteer\Enum;

class Gender {
    const UNKOWN = "unknown";
    const MALE = "male";
    const FEMALE = "female";
    const OTHER = "other";

    /**
     * @return array<string>
     */
    public static function getAvailableTypes() {
        return [
            self::UNKOWN,
            self::MALE,
            self::FEMALE,
            self::OTHER
        ];
    }
}
