<?php

namespace App\Volunteer\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Volunteer\Repository\VolunteerDigestRepository")
 */
class VolunteerDigest {
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private ?string $recipient;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $lastMessageTime;

    public function getRecipient(): ?string {
        return $this->recipient;
    }

    public function setRecipient(string $recipient): self {
        $this->recipient = $recipient;

        return $this;
    }

    public function getLastMessageTime(): ?DateTimeInterface {
        return $this->lastMessageTime;
    }

    public function setLastMessageTime(DateTimeInterface $lastMessageTime): self {
        $this->lastMessageTime = $lastMessageTime;

        return $this;
    }
}
