<?php

namespace App\Volunteer\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Volunteer\Repository\ShiftVolunteerRepository")
 */
class ShiftVolunteer {
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Volunteer\Entity\Shift", inversedBy="volunteers")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Shift $shift = null;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Volunteer\Entity\Volunteer", inversedBy="shifts")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Volunteer $volunteer = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $start = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $end = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $confirmed = null;

    public function getShift(): ?Shift {
        return $this->shift;
    }

    public function setShift(?Shift $shift): self {
        $this->shift = $shift;

        return $this;
    }

    public function getVolunteer(): ?Volunteer {
        return $this->volunteer;
    }

    public function setVolunteer(?Volunteer $volunteer): self {
        $this->volunteer = $volunteer;

        return $this;
    }

    public function getStart(): ?DateTimeInterface {
        return $this->start;
    }

    public function setStart(?DateTimeInterface $start): self {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?DateTimeInterface {
        return $this->end;
    }

    public function setEnd(?DateTimeInterface $end): self {
        $this->end = $end;

        return $this;
    }

    public function getConfirmed(): ?bool {
        return $this->confirmed;
    }

    public function setConfirmed(?bool $confirmed): self {
        $this->confirmed = $confirmed;

        return $this;
    }
}
