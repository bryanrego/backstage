<?php

namespace App\Volunteer\Entity;

use App\Festival\Entity\FestivalDay;
use App\Security\Entity\User;
use App\Volunteer\Enum\Gender;
use App\Volunteer\Enum\VolunteerStatus;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Volunteer\Repository\VolunteerRepository")
 */
class Volunteer {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Security\Entity\User", inversedBy="volunteer", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private ?string $firstname = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private ?string $surname = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $street = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $zip = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $city = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $phone = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private ?string $gender = null;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    private ?DateTimeInterface $birthday = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $shirtsize = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Festival\Entity\FestivalDay")
     * @ORM\JoinTable(name="volunteer_days")
     */
    private Collection $days;

    /**
     * @ORM\ManyToMany(targetEntity="App\Volunteer\Entity\ShiftArea")
     * @ORM\JoinTable(name="volunteer_preferred_areas")
     */
    private Collection $preferredAreas;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $experience = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $comments = null;

    /**
     * @ORM\Column(type="datetime", options={"default":"2019-01-01 00:00:00"})
     */
    private ?DateTimeInterface $appliedAt = null;

    /**
     * @ORM\Column(type="string", length=255, enumType=VolunteerStatus::class)
     * @Assert\NotBlank
     */
    private ?VolunteerStatus $status = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $notes = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Volunteer\Entity\ShiftVolunteer", mappedBy="volunteer", orphanRemoval=true, cascade={"persist"})
     * @var $shifts ShiftVolunteer[]
     */
    private array|Collection $shifts;

    /**
     * @ORM\ManyToMany(targetEntity="App\Volunteer\Entity\VolunteerGroup", mappedBy="volunteers")
     */
    private Collection $groups;

    public function __construct() {
        $this->days = new ArrayCollection();
        $this->preferredAreas = new ArrayCollection();
        $this->shifts = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getFirstname(): ?string {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self {
        $this->firstname = $firstname;

        return $this;
    }

    public function getSurname(): ?string {
        return $this->surname;
    }

    public function setSurname(string $surname): self {
        $this->surname = $surname;

        return $this;
    }

    public function getStreet(): ?string {
        return $this->street;
    }

    public function setStreet(?string $street): self {
        $this->street = $street;

        return $this;
    }

    public function getZip(): ?string {
        return $this->zip;
    }

    public function setZip(?string $zip): self {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string {
        return $this->city;
    }

    public function setCity(?string $city): self {
        $this->city = $city;

        return $this;
    }

    public function getPhone(): ?string {
        return $this->phone;
    }

    public function setPhone(?string $phone): self {
        $this->phone = $phone;

        return $this;
    }

    public function getGender(): ?string {
        return $this->gender;
    }

    public function setGender(string $gender): self {
        if (!in_array($gender, Gender::getAvailableTypes()))
            throw new InvalidArgumentException("Invalid gender");

        $this->gender = $gender;

        return $this;
    }

    public function getBirthday(): ?DateTimeInterface {
        return $this->birthday;
    }

    public function setBirthday(DateTimeInterface $birthday): self {
        $this->birthday = $birthday;

        return $this;
    }

    public function getShirtsize(): ?string {
        return $this->shirtsize;
    }

    public function setShirtsize(?string $shirtsize): self {
        $this->shirtsize = $shirtsize;

        return $this;
    }

    /**
     * @return Collection|FestivalDay[]
     */
    public function getDays(): Collection {
        return $this->days;
    }

    public function setDays(Collection $days): self {
        $this->days = $days;
        return $this;
    }

    public function addDay(FestivalDay $day): self {
        if (!$this->days->contains($day))
            $this->days[] = $day;

        return $this;
    }

    public function removeDay(FestivalDay $day): self {
        $this->days->removeElement($day);

        return $this;
    }

    /**
     * @return Collection|ShiftArea[]
     */
    public function getPreferredAreas(): Collection {
        return $this->preferredAreas;
    }

    public function setPreferredAreas(Collection $areas): self {
        $this->preferredAreas = $areas;
        return $this;
    }

    public function addPreferredArea(ShiftArea $area): self {
        if (!$this->preferredAreas->contains($area))
            $this->preferredAreas[] = $area;

        return $this;
    }

    public function removePreferredArea(ShiftArea $area): self {
        $this->preferredAreas->removeElement($area);

        return $this;
    }

    public function getExperience(): ?string {
        return $this->experience;
    }

    public function setExperience(?string $experience): self {
        $this->experience = $experience;

        return $this;
    }

    public function getComments(): ?string {
        return $this->comments;
    }

    public function setComments(?string $comments): self {
        $this->comments = $comments;

        return $this;
    }

    public function getAppliedAt(): ?DateTimeInterface {
        return $this->appliedAt;
    }

    public function setAppliedAt($appliedAt): self {
        $this->appliedAt = $appliedAt;

        return $this;
    }

    public function getStatus(): ?VolunteerStatus {
        return $this->status;
    }

    public function setStatus(VolunteerStatus $status): self {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ShiftVolunteer[]
     */
    public function getShifts(): Collection {
        return $this->shifts;
    }

    public function addShift(ShiftVolunteer $shift): self {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setVolunteer($this);
        }

        return $this;
    }

    public function removeShift(ShiftVolunteer $shift): self {
        if ($this->shifts->contains($shift)) {
            $this->shifts->removeElement($shift);
            // set the owning side to null (unless already changed)
            if ($shift->getVolunteer() === $this) {
                $shift->setVolunteer(null);
            }
        }

        return $this;
    }

    public function getShiftVolunteer(Shift $shift): ?ShiftVolunteer {
        foreach ($this->shifts as $s) {
            if ($s->getShift()->getId() === $shift->getId())
                return $s;
        }
        return null;
    }

    /**
     * Selects all shifts of this volunteer in the same day as the given shift
     * @param Shift $shift another shift
     * @return ShiftVolunteer[] all shifts of this volunteer in the same area and on the same day as the given shift
     */
    public function getOtherShifts(Shift $shift): array {
        $shifts = [];
        foreach ($this->shifts as $s) {
            if ($s->getShift()->getDay()->getId() === $shift->getDay()->getId())
                $shifts[] = $s;
        }
        return $shifts;
    }

    /**
     * Tests if a shift conflicts with a shift of this volunteer
     * @param Shift $shift a shift
     * @return bool true if the given shift overlaps with another shift of this volunteer
     */
    public function hasConflictingShift(Shift $shift): bool {
        foreach ($this->shifts as $sv) {
            $start = $sv->getShift()->getStart();
            $end = $sv->getShift()->getEnd();
            if ($sv->getShift()->getId() !== $shift->getId() && (                       // if this is a different shift and
                    ($start >= $shift->getStart() && $start < $shift->getEnd()) ||      // its start is during the other shift or
                    ($end > $shift->getStart() && $end <= $shift->getEnd()) ||          // its end is during the other shift or
                    ($start <= $shift->getStart() && $end >= $shift->getEnd())))        // it contains the other shift
                return true;
        }
        return false;
    }

    /**
     * @return Collection|VolunteerGroup[]
     */
    public function getGroups(): Collection {
        return $this->groups;
    }

    public function addGroup(VolunteerGroup $group): self {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->addVolunteer($this);
        }

        return $this;
    }

    public function removeGroup(VolunteerGroup $group): self {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
            $group->removeVolunteer($this);
        }

        return $this;
    }

    public function getNotes(): ?string {
        return $this->notes;
    }

    public function setNotes(?string $notes): self {
        $this->notes = $notes;

        return $this;
    }
}
