<?php

namespace App\Volunteer\Repository;

use App\Festival\Entity\Festival;
use App\Volunteer\Entity\ShiftVolunteer;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Enum\ShiftAreaType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShiftVolunteer|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShiftVolunteer|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShiftVolunteer[]    findAll()
 * @method ShiftVolunteer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShiftVolunteerRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ShiftVolunteer::class);
    }

    public function findAllWhereAreaTypeAndFestivalAndVolunteer(ShiftAreaType $type, Festival $festival, Volunteer $volunteer) {
        return $this->createQueryBuilder('sv')
            ->select('sv', 's', 'a', 'd')
            ->join('sv.volunteer', 'v')
            ->join('sv.shift', 's')
            ->join('s.area', 'a')
            ->join('s.day', 'd')
            ->where('a.type = :type', 'd.festival = :festival', 'v = :volunteer')
            ->orderBy('s.start')
            ->setParameters([
                'type' => $type->value,
                'festival' => $festival,
                'volunteer' => $volunteer
            ])
            ->getQuery()
            ->getResult();
    }
}
