<?php

namespace App\Volunteer\Repository;

use App\Volunteer\Entity\ShiftArea;
use App\Volunteer\Enum\ShiftAreaType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShiftArea|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShiftArea|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShiftArea[]    findAll()
 * @method ShiftArea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShiftAreaRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ShiftArea::class);
    }

    /**
     * @param ShiftAreaType $type shift area type to find
     * @return ShiftArea[] all shift areas of the given type
     */
    public function findWhereTypeOrderedByName(ShiftAreaType $type): array {
        return $this->createQueryBuilder('a')
            ->where('a.type = :type')
            ->orderBy('a.name')
            ->setParameter('type', $type->value)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ShiftAreaType[] $types shift area types to find
     * @return ShiftArea[] all shift areas of the given types
     */
    public function findWhereTypeInOrderedByName(array $types): array {
        return $this->createQueryBuilder('a')
            ->where('a.type IN (:types)')
            ->orderBy('a.type')
            ->addOrderBy('a.name')
            ->setParameter('types', array_map(function (ShiftAreaType $type) {
                return $type->value;
            }, $types))
            ->getQuery()
            ->getResult();
    }
}
