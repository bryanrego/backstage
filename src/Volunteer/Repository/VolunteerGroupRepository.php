<?php

namespace App\Volunteer\Repository;

use App\Volunteer\Entity\VolunteerGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VolunteerGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method VolunteerGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method VolunteerGroup[]    findAll()
 * @method VolunteerGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VolunteerGroupRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, VolunteerGroup::class);
    }

    /**
     * Finds all volunteer groups, joined with volunteers
     *
     * @return array all volunteer groups
     */
    public function findAllJoinVolunteers(): array {
        return $this->createQueryBuilder('g')
            ->select('g', 'v')
            ->leftJoin('g.volunteers', 'v')
            ->orderBy('g.name')
            ->getQuery()
            ->getResult();
    }
}
