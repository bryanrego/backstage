<?php

namespace App\Volunteer\Form\DataMapper;

use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftVolunteer;
use App\Volunteer\Entity\Volunteer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;
use Traversable;

class VolunteerMapper implements DataMapperInterface {
    private EntityManagerInterface $doctrine;

    public function __construct(EntityManagerInterface $doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @var FormInterface[]|Traversable $forms
     */
    public function mapDataToForms($data, $forms) {
        if ($data === null)
            return;

        if (!$data instanceof Volunteer)
            throw new UnexpectedTypeException($data, Volunteer::class);

        $forms = iterator_to_array($forms);

        $forms['firstname']->setData($data->getFirstname());
        $forms['surname']->setData($data->getSurname());
        $forms['street']->setData($data->getStreet());
        $forms['zip']->setData($data->getZip());
        $forms['city']->setData($data->getCity());
        $forms['phone']->setData($data->getPhone());
        $forms['gender']->setData($data->getGender());
        $forms['birthday']->setData($data->getBirthday());
        $forms['shirtsize']->setData($data->getShirtsize());
        $forms['days']->setData($data->getDays());
        $forms['preferredAreas']->setData($data->getPreferredAreas());
        $forms['experience']->setData($data->getExperience());
        $forms['comments']->setData($data->getComments());

        $construction = [];
        $allConstructions = $forms['construction']->all();
        $dismantling = [];
        $allDismantlings = $forms['dismantling']->all();
        foreach ($data->getShifts() as $shift) {
            $id = $shift->getShift()->getId();
            if (array_key_exists($id, $allConstructions)) {
                $construction[$id] = [
                    'date' => true,
                    'start' => $shift->getStart()->format('H'),
                    'end' => $shift->getEnd()->format('H')
                ];
            }
            if (array_key_exists($id, $allDismantlings)) {
                $dismantling[$id] = [
                    'date' => true,
                    'start' => $shift->getStart()->format('H'),
                    'end' => $shift->getEnd()->format('H')
                ];
            }
        }
        $forms['construction']->setData($construction);
        $forms['dismantling']->setData($dismantling);
    }

    /**
     * @param Volunteer $data
     */
    public function mapFormsToData($forms, &$data) {
        $forms = iterator_to_array($forms);

        $data->setFirstname($forms['firstname']->getData());
        $data->setSurname($forms['surname']->getData());
        $data->setStreet($forms['street']->getData());
        $data->setZip($forms['zip']->getData());
        $data->setCity($forms['city']->getData());
        $data->setPhone($forms['phone']->getData());
        $data->setGender($forms['gender']->getData());
        $data->setBirthday($forms['birthday']->getData());
        $data->setShirtsize($forms['shirtsize']->getData());
        $data->setDays($forms['days']->getData());
        $data->setPreferredAreas($forms['preferredAreas']->getData());
        $data->setExperience($forms['experience']->getData());
        $data->setComments($forms['comments']->getData());

        foreach ($forms['construction']->getData() as $id => $entry) {
            $this->mapShift($id, $entry, $data);
        }

        foreach ($forms['dismantling']->getData() as $id => $entry) {
            $this->mapShift($id, $entry, $data);
        }
    }

    private function mapShift(int $id, array $entry, Volunteer &$data) {
        $sv = $data->getShifts()->filter(function (ShiftVolunteer $shift) use ($id) {
            return $shift->getShift()->getId() == $id;
        })->first();
        if (isset($entry['date']) && $entry['date'] === true) {
            // check if this shift already exists
            if ($sv !== false) {      // update times
                $start = clone $sv->getStart();
                $start->setTime($entry['start'], 0);
                $sv->setStart($start);
                $end = clone $sv->getEnd();
                $end->setTime($entry['end'], 0);
                $sv->setEnd($end);
            } else {                  // add new
                /** @var Shift $shift */
                $shift = $this->doctrine->find('\App\Volunteer\Entity\Shift', $id);
                if ($shift === null)
                    throw new TransformationFailedException("Cannot find any shift with id " . $id);

                $sv = new ShiftVolunteer();
                $sv->setVolunteer($data);
                $sv->setShift($shift);
                $start = clone $shift->getStart();
                $start->setTime($entry['start'], 0);
                $sv->setStart($start);
                $end = clone $shift->getEnd();
                $end->setTime($entry['end'], 0);
                $sv->setEnd($end);

                $data->addShift($sv);
            }
        } else {
            if ($sv)
                $data->removeShift($sv);
        }
    }
}
