<?php

namespace App\Volunteer\Form;

use App\Volunteer\Entity\Shift;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShiftType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        /** @var Shift $shift */
        $shift = $options['shift'];
        if ($options['flexible']) {
            $builder
                ->add('date', CheckboxType::class, [
                    'label' => $shift->getStart()->format('l, d.m.')
                ])
                ->add('start', ChoiceType::class, [
                    'choices' => range($shift->getStart()->format('H'), $shift->getEnd()->format('H') - 1),
                    'choice_label' => function ($value) {
                        return str_pad($value, 2, '0', STR_PAD_LEFT) . ' Uhr';
                    },
                    'label' => 'von '
                ])
                ->add('end', ChoiceType::class, [
                    'choices' => array_reverse(range((int)$shift->getStart()->format('H') + 1, $shift->getEnd()->format('H'))),
                    'choice_label' => function ($value) {
                        return str_pad($value, 2, '0', STR_PAD_LEFT) . ' Uhr';
                    },
                    'label' => 'bis '
                ]);
        } else {
            $builder->add('date', CheckboxType::class, [
                'label' => $shift->getStart()->format('D, d.m. H:i') . ' - ' . $shift->getEnd()->format('H:i')
            ]);
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['shift'] = $options['shift'];
        $view->vars['flexible'] = $options['flexible'];
        $view->vars['data'] = $form->getData();
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefined([
            'shift',
            'flexible'
        ]);
        $resolver->setAllowedTypes('shift', 'App\Volunteer\Entity\Shift');
        $resolver->setAllowedTypes('flexible', 'bool');
        $resolver->setRequired('shift');
        $resolver->setDefaults([
            'flexible' => false,
            'error_mapping' => [
                '.' => 'date'
            ]
        ]);
    }

    public function getParent(): string {
        return FormType::class;
    }
}
