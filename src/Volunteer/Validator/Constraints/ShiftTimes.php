<?php

namespace App\Volunteer\Validator\Constraints;

use App\Volunteer\Entity\Shift;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ShiftTimes extends Constraint {
    public string $message = 'shift.invalid_times';
    public Shift $shift;

    public function getTargets(): array|string {
        return self::CLASS_CONSTRAINT;
    }

}
