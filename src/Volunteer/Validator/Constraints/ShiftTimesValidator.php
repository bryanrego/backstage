<?php

namespace App\Volunteer\Validator\Constraints;

use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ShiftTimesValidator extends ConstraintValidator {
    public function validate($value, Constraint $constraint) {
        if (!$constraint instanceof ShiftTimes)
            throw new UnexpectedTypeException($constraint, ShiftTimes::class);
        if ($value === null)
            return;

        if (!is_array($value) || !isset($value['date']) || !isset($value['start']) || !isset($value['end'])) {
            $this->addViolation($constraint);
            return;
        }

        $date = $value['date'];
        if ($date !== true)
            return;

        $start = $value['start'];
        $end = $value['end'];
        if (!is_numeric($start) || !is_numeric($end)) {
            $this->addViolation($constraint);
            return;
        }

        $shift = $constraint->shift;
        $startTime = clone $shift->getStart();
        $startTime->setTime($start, 0);
        $endTime = clone $shift->getEnd();
        $endTime->setTime($end, 0);

        if ($startTime < $shift->getStart() || $endTime > $shift->getEnd() || $startTime >= $endTime) {
            $this->addViolation($constraint);
        }
    }

    private function addViolation(ShiftTimes $constraint) {
        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}