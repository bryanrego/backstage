<?php

namespace App\Inventory;

use App\System\Exception\InternalServerErrorHttpException;

class ItemNumberDuplicateException extends InternalServerErrorHttpException {

}
