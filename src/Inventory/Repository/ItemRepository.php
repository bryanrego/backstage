<?php

namespace App\Inventory\Repository;

use App\Inventory\Entity\Item;
use App\Inventory\Entity\ItemType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Item::class);
    }

    public function findWhereNumber(int $number): ?Item {
        return $this->createQueryBuilder('i')
            ->where('i.number = :number')
            ->setParameter('number', $number)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Finds an item which does not have the given id but does have the given number
     *
     * @param ?int $id id the item should not have or null to search for any item with the given number
     * @param int $number number of the item
     * @return Item the item found
     */
    public function findWhereNotIdAndNumber(?int $id, int $number): ?Item {
        $query = $this->createQueryBuilder('i')
            ->where('i.number = :number');
        if (!is_null($id)) {
            $query->andWhere('i.id != :id')
                ->setParameter('id', $id);
        }
        return $query
            ->setParameter('number', $number)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Finds all items of the given type having an id from the given array.
     *
     * @param ItemType $type type of the items
     * @param string[]|int[] $ids ids of items to find
     * @return Item[] all items found
     */
    public function findAllWhereTypeAndIdIn(ItemType $type, array $ids): array {
        return $this->createQueryBuilder('i')
            ->where('i.type = :type', 'i.id IN (:ids)')
            ->orderBy('i.id')
            ->setParameters([
                'type' => $type,
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * Finds the amount of items that have a number in the given range
     *
     * @param int $start start of range (inclusive)
     * @param int $end end of range (inclusive)
     * @return int amount of items in the given range
     */
    public function findCountWhereNumberBetween(int $start, int $end): int {
        return $this->createQueryBuilder('i')
            ->select('COUNT(i.id)')
            ->where('i.number >= :start', 'i.number <= :end')
            ->setParameters([
                'start' => $start,
                'end' => $end
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Finds all borrowed for fields
     *
     * @return string[] all distinct borrowed for fields
     */
    public function findAllBorrowedForDistinct(): array {
        return $this->createQueryBuilder('i')
            ->select('i.borrowedFor')
            ->where('i.borrowedFor IS NOT NULL')
            ->distinct()
            ->orderBy('i.borrowedFor')
            ->getQuery()
            ->getSingleColumnResult();
    }
}
