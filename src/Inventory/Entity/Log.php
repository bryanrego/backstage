<?php

namespace App\Inventory\Entity;

use App\Inventory\Enum\LogAction;
use App\Security\Entity\User;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Inventory\Repository\LogRepository")
 * @ORM\Table(name="inventory_log", indexes={
 *     @ORM\Index(columns={"action"})
 * })
 */
class Log {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Inventory\Entity\ItemType", inversedBy="log")
     * @ORM\JoinColumn(nullable=false)
     */
    private ItemType $itemType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Inventory\Entity\Item")
     */
    private ?Item $item;

    /**
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private User $user;

    /**
     * @ORM\Column(type="string", length=255, enumType=LogAction::class)
     */
    private LogAction $action;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $timestamp;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $oldValue;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $newValue;

    public function __construct(ItemType $type, ?Item $item, User $user, LogAction $action, $oldValue = null, $newValue = null) {
        $this->itemType = $type;
        $this->item = $item;
        $this->user = $user;
        $this->action = $action;
        $this->timestamp = new DateTimeImmutable();
        if (is_bool($oldValue))
            $oldValue = $oldValue ? '1' : '0';
        $this->oldValue = $oldValue;
        if (is_bool($newValue))
            $newValue = $newValue ? '1' : '0';
        $this->newValue = $newValue;
        $type->addLog($this);
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getItemType(): ?ItemType {
        return $this->itemType;
    }

    public function setItemType(?ItemType $itemType): self {
        $this->itemType = $itemType;

        return $this;
    }

    public function getItem(): ?Item {
        return $this->item;
    }

    public function setItem(?Item $item): self {
        $this->item = $item;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getAction(): ?LogAction {
        return $this->action;
    }

    public function setAction(LogAction $action): self {
        $this->action = $action;

        return $this;
    }

    public function getTimestamp(): ?DateTimeInterface {
        return $this->timestamp;
    }

    public function setTimestamp(DateTimeInterface $timestamp): self {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getOldValue(): ?string {
        return $this->oldValue;
    }

    public function setOldValue(?string $oldValue): self {
        $this->oldValue = $oldValue;

        return $this;
    }

    public function getNewValue(): ?string {
        return $this->newValue;
    }

    public function setNewValue(?string $newValue): self {
        $this->newValue = $newValue;

        return $this;
    }
}
