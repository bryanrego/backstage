<?php

namespace App\System\Entity;

use App\Security\Entity\User;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\System\Repository\EmailRecordRepository")
 */
class EmailRecord {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $sender = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $receiver = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $sent_at = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $subject = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $text = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getSender(): ?User {
        return $this->sender;
    }

    public function setSender(?User $sender): self {
        $this->sender = $sender;

        return $this;
    }

    public function getReceiver(): ?User {
        return $this->receiver;
    }

    public function setReceiver(?User $receiver): self {
        $this->receiver = $receiver;

        return $this;
    }

    public function getSentAt(): ?DateTimeInterface {
        return $this->sent_at;
    }

    public function setSentAt(DateTimeInterface $sent_at): self {
        $this->sent_at = $sent_at;

        return $this;
    }

    public function getSubject(): ?string {
        return $this->subject;
    }

    public function setSubject(string $subject): self {
        $this->subject = $subject;

        return $this;
    }

    public function getText(): ?string {
        return $this->text;
    }

    public function setText(string $text): self {
        $this->text = $text;

        return $this;
    }
}
