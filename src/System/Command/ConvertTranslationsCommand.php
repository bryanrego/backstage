<?php

namespace App\System\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class ConvertTranslationsCommand extends Command {
    protected static $defaultName = 'app:convert-translations';

    protected function configure(): void {
        $this
            ->setDescription('Dumps all translations as yaml')
            ->addArgument('file', InputArgument::REQUIRED, 'The file to convert');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $file = 'translations' . DIRECTORY_SEPARATOR . $input->getArgument('file') . '.php';
        if (!is_file($file))
            return Command::FAILURE;

        $messages = require($file);
        $converted = [];
        foreach ($messages as $key => $message) {
            $this->splitAndAdd($key, $message, $converted, $output);
        }

        $yaml = Yaml::dump($converted, 999, 2);
        $info = pathinfo($file);
        file_put_contents($info['dirname'] . DIRECTORY_SEPARATOR . $info['filename'] . '.yaml', $yaml);

        return Command::SUCCESS;
    }

    private function splitAndAdd(string $key, string $message, array &$array, OutputInterface $output): void {
        $parts = explode('.', $key, 2);
        $first = array_shift($parts);
        // if there are more parts repeat recursively
        if (!empty($parts)) {
            // insert empty array if necessary
            if (!array_key_exists($first, $array)) {
                $array[$first] = [];
            } else if (!is_array($array[$first])) {
                // if the key already exists but is not another array, we have a conflict
                $output->writeln('key conflict: ' . $key);
                // move the conflicting object to a new key
                $array[$first . '_'] = $array[$first];
                $array[$first] = [];
            }
            // repeat with remaining key
            $this->splitAndAdd($parts[0], $message, $array[$first], $output);
        } else {
            // add message if this was the last key part
            $array[$first] = $message;
        }
        // sort array by keys at the end
        ksort($array);
    }
}
