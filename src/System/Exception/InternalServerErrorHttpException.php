<?php

namespace App\System\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Signals an internal server error. Displays a clean message to the client.
 */
class InternalServerErrorHttpException extends HttpException {
    public function __construct(string $message = 'Internal Server Error', Throwable $previous = null, int $code = 0, array $headers = []) {
        parent::__construct(500, $message, $previous, $headers, $code);
    }
}
