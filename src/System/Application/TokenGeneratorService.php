<?php

namespace App\System\Application;

class TokenGeneratorService {

    public function generateToken($length): string {
        $keyspace = ConfigService::TOKEN_KEYSPACE;

        $pieces = [];
        $max = strlen($keyspace) - 1;
        for ($i = 0; $i < $length; $i++) {
            $pieces [] = $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

}
