<?php

namespace App\System\Application;

use App\Festival\Entity\Festival;
use App\Festival\Repository\FestivalRepository;
use App\System\Entity\Configuration;
use App\System\Exception\InternalServerErrorHttpException;
use App\System\Repository\ConfigurationRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ConfigService {
    public const PASSWORD_MIN_LENGTH = 6;
    public const PASSWORD_MAX_LENGTH = 51;
    public const PASSWORD_HASH_STRENGTH = 12;
    public const EMAIL_REGEX = "^[a-zA-Z0-9\._%+-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}$";
    public const URL_REGEX = "https?://[-_./a-z0-9]+\.[a-z]{2,4}[-_./?&=%#a-z0-9]*";
    public const TOKEN_KEYSPACE = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
    public const PAGE_SIZE = 50;

    private EntityManagerInterface $doctrine;
    private ConfigurationRepository $repository;
    private FestivalRepository $festivalRepository;
    private ?array $values;

    private ?Festival $currentFestival = null;

    public function __construct(EntityManagerInterface $doctrine, ConfigurationRepository $repository, FestivalRepository $festivalRepository) {
        $this->doctrine = $doctrine;
        $this->repository = $repository;
        $this->festivalRepository = $festivalRepository;
        $this->values = null;
    }

    private function fetch(): void {
        $entries = $this->repository->findAll();
        $this->values = array();
        foreach ($entries as $entry) {
            $this->values[$entry->getId()] = $entry->getValue();
        }
    }

    public function get(string $key, $def = null): ?string {
        if ($this->values === null)
            $this->fetch();
        if (empty($this->values[$key])) {
            if ($def === null)
                throw new InternalServerErrorHttpException('Unknown config value "' . $key . '"');
            return $def;
        }
        return $this->values[$key];
    }

    public function save(string $key, string $value): void {
        $entry = new Configuration();
        $entry->setId($key)->setValue($value);
        $this->doctrine->persist($entry);
    }

    /**
     * Gets the value of the given key as a {@link DateTimeImmutable} object.
     *
     * @param string $key The key to look up
     * @return DateTimeImmutable parsed datetime object
     */
    public function getDateTime(string $key): DateTimeImmutable {
        $value = $this->get($key);
        try {
            return new DateTimeImmutable($value);
        } catch (Exception $e) {
            throw new InternalServerErrorHttpException('Cannot parse config value for "' . $key . '" as datetime object: ' . $value, $e);
        }
    }

    /**
     * Gets the current festival as an object.
     *
     * @return Festival the current festival
     */
    public function getCurrentFestival(): Festival {
        if ($this->currentFestival === null) {
            $id = (int)$this->get('current_festival');
            if (empty($id))
                throw new InternalServerErrorHttpException('Current festival is not configured');
            $festival = $this->festivalRepository->find($id);
            if ($festival == null)
                throw new NotFoundHttpException('Cannot find festival with id ' . $id);
            $this->currentFestival = $festival;
        }
        return $this->currentFestival;
    }
}