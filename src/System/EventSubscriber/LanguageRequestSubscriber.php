<?php

namespace App\System\EventSubscriber;

use App\Security\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

/**
 * Updates the preferred language if the `lang` parameter is set in the request. Defaults to the session value.
 */
class LanguageRequestSubscriber implements EventSubscriberInterface {
    private RequestStack $requestStack;
    private Security $security;
    private EntityManagerInterface $doctrine;

    public function __construct(RequestStack $requestStack, Security $security, EntityManagerInterface $doctrine) {
        $this->requestStack = $requestStack;
        $this->security = $security;
        $this->doctrine = $doctrine;
    }

    /**
     * Sets the locale for the request. Uses the `lang` query parameter if it is present, otherwise
     * the current session value is used.
     */
    public function onKernelRequest(RequestEvent $event) {
        $request = $event->getRequest();
        $lang = $request->get('lang');
        if ($lang === 'de' || $lang === 'en') {
            $this->requestStack->getSession()->set('_locale', $lang);
        } else {
            $lang = $this->requestStack->getSession()->get('_locale', 'de');
        }
        $request->setLocale($lang);
        switch ($lang) {
            case 'de':
                setlocale(LC_ALL, 'de', 'de_DE', 'de_DE.utf8');
                break;
            case 'en':
                setlocale(LC_ALL, 'en', 'en_GB', 'en_GB.utf8');
                break;
        }
    }

    /**
     * Saves the locale in the user entity if it does not match the current value
     */
    public function onKernelController(ControllerEvent $event) {
        if ($this->security->isGranted('IS_AUTHENTICATED')) {
            $locale = $event->getRequest()->getLocale();
            /** @var User $user */
            $user = $this->security->getUser();
            if ($locale !== $user->getLang()) {
                $user->setLang($locale);
                $this->doctrine->flush();
            }
        }
    }

    public static function getSubscribedEvents(): array {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 20],
            KernelEvents::CONTROLLER => ['onKernelController', 10]
        ];
    }
}
