<?php

namespace App\System\EventSubscriber;

use App\Security\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * Updates the last_login value for users upon login.
 */
class LoginSubscriber implements EventSubscriberInterface {
    private EntityManagerInterface $doctrine;
    private RequestStack $requestStack;

    public function __construct(EntityManagerInterface $doctrine, RequestStack $requestStack) {
        $this->doctrine = $doctrine;
        $this->requestStack = $requestStack;
    }

    public function onLogin(InteractiveLoginEvent $event) {
        /** @var User $user */
        $user = $event->getAuthenticationToken()->getUser();

        $user->setLastLogin(new DateTimeImmutable());
        $this->doctrine->persist($user);
        $this->doctrine->flush();

        $this->requestStack->getSession()->set('_locale', $user->getLang());
    }

    public static function getSubscribedEvents(): array {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'onLogin'
        ];
    }
}
