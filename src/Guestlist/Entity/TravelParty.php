<?php

namespace App\Guestlist\Entity;

use App\Festival\Entity\FestivalDay;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * @ORM\Entity(repositoryClass="App\Guestlist\Repository\TravelPartyRepository")
 */
class TravelParty {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Guestlist\Entity\Band", inversedBy="travelParties")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Band $band = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Festival\Entity\FestivalDay", inversedBy="travelParties")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?FestivalDay $day = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $vehicle = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $stageTime = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $checkedIn = false;

    /**
     * @ORM\ManyToMany(targetEntity="App\Guestlist\Entity\Guestlist", inversedBy="travelParties")
     * @ORM\JoinTable(name="travel_party_guestlist", joinColumns={
     *     @ORM\JoinColumn(name="travel_party_id", referencedColumnName="id", onDelete="CASCADE"),
     * }, inverseJoinColumns={
     *     @ORM\JoinColumn(name="guestlist_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private Collection $guestlist;

    #[Pure]
    public function __construct() {
        $this->guestlist = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getBand(): ?Band {
        return $this->band;
    }

    public function setBand(Band $band): self {
        $this->band = $band;

        return $this;
    }

    public function getDay(): ?FestivalDay {
        return $this->day;
    }

    public function setDay(?FestivalDay $day): self {
        $this->day = $day;

        return $this;
    }

    public function getVehicle(): ?string {
        return $this->vehicle;
    }

    public function setVehicle(?string $vehicle): self {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getStageTime(): ?DateTimeInterface {
        return $this->stageTime;
    }

    public function setStageTime(DateTimeInterface $stageTime): self {
        $this->stageTime = $stageTime;

        return $this;
    }

    public function getCheckedIn(): ?bool {
        return $this->checkedIn;
    }

    public function setCheckedIn(?bool $checkedIn): self {
        $this->checkedIn = $checkedIn;

        return $this;
    }

    /**
     * @return Collection|Guestlist[]
     */
    public function getGuestlist(): Collection {
        return $this->guestlist;
    }

    public function addGuestlist(Guestlist $entry): self {
        if (!$this->guestlist->contains($entry)) {
            $this->guestlist[] = $entry;
            $entry->addTravelParty($this);
        }

        return $this;
    }

    public function removeGuestlist(Guestlist $entry): self {
        if ($this->guestlist->contains($entry)) {
            $this->guestlist->removeElement($entry);
            // set the owning side to null (unless already changed)
            if ($entry->getTravelParties()->contains($this))
                $entry->removeTravelParty($this);
        }

        return $this;
    }
}
