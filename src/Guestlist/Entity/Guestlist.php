<?php

namespace App\Guestlist\Entity;

use App\Festival\Entity\Festival;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Guestlist\Repository\GuestlistRepository")
 */
class Guestlist {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Festival\Entity\Festival")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Festival $festival = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Guestlist\Entity\GuestlistDay", mappedBy="guestlist", orphanRemoval=true,
     *     fetch="EAGER", cascade={"persist"})
     */
    private Collection $days;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $comment = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $camping = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $origin = null;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\LessThanOrEqual(3)
     */
    private ?int $plus = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $checkedIn = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $backstageCheckedIn = false;

    /**
     * @ORM\ManyToMany(targetEntity="App\Guestlist\Entity\TravelParty", mappedBy="guestlist")
     */
    private Collection $travelParties;

    public function __construct() {
        $this->days = new ArrayCollection();
        $this->travelParties = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getFestival(): ?Festival {
        return $this->festival;
    }

    public function setFestival(?Festival $festival): self {
        $this->festival = $festival;

        return $this;
    }

    public function getDays(): ArrayCollection|Collection {
        return $this->days;
    }

    public function addDay(GuestlistDay $day): self {
        if (!$this->days->contains($day)) {
            $this->days[] = $day;
            $day->setGuestlist($this);
        }

        return $this;
    }

    public function removeDay(GuestlistDay $day): self {
        if ($this->days->contains($day)) {
            $this->days->removeElement($day);
            // set the owning side to null (unless already changed)
            if ($day->getGuestlist() === $this) {
                $day->setGuestlist(null);
            }
        }

        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(?string $comment): self {
        $this->comment = $comment;

        return $this;
    }

    public function getCamping(): ?bool {
        return $this->camping;
    }

    public function setCamping(?bool $camping): self {
        $this->camping = $camping;

        return $this;
    }

    public function getOrigin(): ?string {
        return $this->origin;
    }

    public function setOrigin(?string $origin): self {
        $this->origin = $origin;

        return $this;
    }

    public function getPlus(): ?int {
        return $this->plus;
    }

    public function setPlus(?int $plus): self {
        $this->plus = $plus;

        return $this;
    }

    public function getCheckedIn(): ?bool {
        return $this->checkedIn;
    }

    public function setCheckedIn(?bool $checkedIn): self {
        $this->checkedIn = $checkedIn;

        return $this;
    }

    function getBackstageCheckedIn(): ?bool {
        return $this->backstageCheckedIn;
    }

    public function setBackstageCheckedIn(?bool $backstageCheckedIn): self {
        $this->backstageCheckedIn = $backstageCheckedIn;

        return $this;
    }

    /**
     * @return Collection|TravelParty[]
     */
    public function getTravelParties(): Collection {
        return $this->travelParties;
    }

    public function addTravelParty(TravelParty $party): self {
        if (!$this->travelParties->contains($party)) {
            $this->travelParties[] = $party;
            $party->addGuestlist($this);
        }

        return $this;
    }

    public function removeTravelParty(TravelParty $party): self {
        if ($this->travelParties->contains($party)) {
            $this->travelParties->removeElement($party);
            // set the owning side to null (unless already changed)
            if ($party->getGuestlist()->contains($this))
                $party->removeGuestlist($this);
        }

        return $this;
    }
}
