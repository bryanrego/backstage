<?php

namespace App\Guestlist\Entity;

use App\Festival\Entity\FestivalDay;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Guestlist\Repository\GuestlistDayRepository")
 */
class GuestlistDay {
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Guestlist\Entity\Guestlist", inversedBy="days")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Guestlist $guestlist = null;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Festival\Entity\FestivalDay")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?FestivalDay $day = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $backstage = null;

    public function getGuestlist(): ?Guestlist {
        return $this->guestlist;
    }

    public function setGuestlist(?Guestlist $guestlist): self {
        $this->guestlist = $guestlist;

        return $this;
    }

    public function getDay(): ?FestivalDay {
        return $this->day;
    }

    public function setDay(?FestivalDay $day): self {
        $this->day = $day;

        return $this;
    }

    public function getBackstage(): ?bool {
        return $this->backstage;
    }

    public function setBackstage(?bool $backstage): self {
        $this->backstage = $backstage;

        return $this;
    }
}
