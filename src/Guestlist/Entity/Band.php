<?php

namespace App\Guestlist\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Guestlist\Repository\BandRepository")
 */
class Band {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $contact_name = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $contact_email = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $contact_phone = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $location = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Guestlist\Entity\TravelParty", mappedBy="band")
     */
    private Collection $travelParties;

    public function __construct() {
        $this->travelParties = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getContactName(): ?string {
        return $this->contact_name;
    }

    public function setContactName(?string $contact_name): self {
        $this->contact_name = $contact_name;

        return $this;
    }

    public function getContactEmail(): ?string {
        return $this->contact_email;
    }

    public function setContactEmail(?string $contact_email): self {
        $this->contact_email = $contact_email;

        return $this;
    }

    public function getContactPhone(): ?string {
        return $this->contact_phone;
    }

    public function setContactPhone(?string $contact_phone): self {
        $this->contact_phone = $contact_phone;

        return $this;
    }

    public function getLocation(): ?string {
        return $this->location;
    }

    public function setLocation(?string $location): self {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection|TravelParty[]
     */
    public function getTravelParties(): Collection {
        return $this->travelParties;
    }

    public function addTravelParty(TravelParty $party): self {
        if (!$this->travelParties->contains($party)) {
            $this->travelParties[] = $party;
            $party->setBand($this);
        }

        return $this;
    }

    public function removeTravelParty(TravelParty $party): self {
        if ($this->travelParties->contains($party)) {
            $this->travelParties->removeElement($party);
            // set the owning side to null (unless already changed)
            if ($party->getBand() === $this)
                $party->setBand(null);
        }

        return $this;
    }
}
