<?php

namespace App\Guestlist\Repository;

use App\Festival\Entity\Festival;
use App\Guestlist\Entity\TravelParty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TravelParty|null find($id, $lockMode = null, $lockVersion = null)
 * @method TravelParty|null findOneBy(array $criteria, array $orderBy = null)
 * @method TravelParty[]    findAll()
 * @method TravelParty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TravelPartyRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, TravelParty::class);
    }

    public function findAllWhereFestival(Festival $festival): array {
        return $this->createQueryBuilder('t')
            ->select('t', 'b', 'g', 'gd')
            ->join('t.band', 'b')
            ->join('t.day', 'd')
            ->leftJoin('t.guestlist', 'g')
            ->leftJoin('g.days', 'gd')
            ->where('d.festival = :festival')
            ->orderBy('b.name')
            ->setParameter('festival', $festival)
            ->getQuery()
            ->getResult();
    }
}
