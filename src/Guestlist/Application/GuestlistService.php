<?php

namespace App\Guestlist\Application;

use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use App\Guestlist\Entity\Band;
use App\Guestlist\Entity\Guestlist;
use App\Guestlist\Entity\GuestlistDay;
use App\Guestlist\Entity\TravelParty;
use App\Guestlist\Repository\BandRepository;
use App\Guestlist\Repository\GuestlistDayRepository;
use App\Guestlist\Repository\GuestlistRepository;
use App\Guestlist\Repository\TravelPartyRepository;
use App\System\Application\MapperService;
use App\System\Application\MercureService;
use Doctrine\ORM\EntityManagerInterface;

class GuestlistService {
    private static array $DTO_GUESTLIST = [
        'id',
        'name',
        'origin',
        'comment',
        'days' => [
            'day' => ['id'],
            'backstage'
        ],
        'camping',
        'plus',
        'checkedIn',
        'backstageCheckedIn'
    ];
    private static array $DTO_BAND = [
        'id',
        'name',
        'contactName',
        'contactEmail',
        'contactPhone',
        'location'
    ];
    private static array $DTO_TRAVELPARTY = [
        'id',
        'band' => [
            'id',
            'name',
            'contactName',
            'contactEmail',
            'contactPhone',
            'location'
        ],
        'day' => ['id'],
        'vehicle',
        'stageTime',
        'checkedIn',
        'guestlist' => [
            'id',
            'name',
            'origin',
            'comment',
            'days' => [
                'day' => ['id'],
                'backstage'
            ],
            'camping',
            'plus',
            'checkedIn',
            'backstageCheckedIn'
        ]
    ];

    private EntityManagerInterface $doctrine;
    private MercureService $mercure;
    private MapperService $mapper;
    private GuestlistRepository $repository;
    private GuestlistDayRepository $dayRepository;
    private BandRepository $bandRepository;
    private TravelPartyRepository $travelPartyRepository;

    public function __construct(
        EntityManagerInterface $doctrine,
        MercureService         $mercure,
        MapperService          $mapper,
        GuestlistRepository    $repository,
        GuestlistDayRepository $dayRepository,
        BandRepository         $bandRepository,
        TravelPartyRepository  $travelPartyRepository
    ) {
        $this->doctrine = $doctrine;
        $this->mercure = $mercure;
        $this->mapper = $mapper;
        $this->repository = $repository;
        $this->dayRepository = $dayRepository;
        $this->bandRepository = $bandRepository;
        $this->travelPartyRepository = $travelPartyRepository;
    }

    /**
     * Finds all guestlist entries for a given festival. Returns an associative array.
     *
     * @param Festival $festival the festival to find
     * @return array all guestlist entries. See {@link $DTO_GUESTLIST} for the structure.
     */
    public function getGuestlist(Festival $festival): array {
        // find all guestlist entries for the given festival
        $guestlists = $this->repository->findAllWhereFestivalOrderedByName($festival);
        // normalize to array with the following values
        return $this->mapper->mapToArrays($guestlists, self::$DTO_GUESTLIST);
    }

    /**
     * Finds all guestlist entries with the given IDs.
     *
     * @param int[] $ids ids of entries
     * @return array all found entries
     */
    public function getGuestlistById(array $ids): array {
        return $this->repository->findAllWhereIdIn($ids);
    }

    /**
     * Finds a guestlist day for a given guestlist entry and festival day
     *
     * @param Guestlist $guestlist guestlist entry to search
     * @param FestivalDay $day festival day to search
     * @return GuestlistDay|null the guestlist day or null if not found
     */
    public function getGuestlistDay(Guestlist $guestlist, FestivalDay $day): ?GuestlistDay {
        return $this->dayRepository->findOneBy([
            'guestlist' => $guestlist,
            'day' => $day
        ]);
    }

    /**
     * Finds all bands. Returns an associative array.
     *
     * @return array all bands. See {@link $DTO_BAND} for the structure.
     */
    public function getBands(): array {
        $bands = $this->bandRepository->findBy([], ['name' => 'ASC']);
        return $this->mapper->mapToArrays($bands, self::$DTO_BAND);
    }

    /**
     * Finds a band by its id.
     *
     * @param int $id id of the band
     * @return Band|null the band if it exists
     */
    public function getBand(int $id): ?Band {
        return $this->bandRepository->find($id);
    }

    /**
     * Finds all travel parties relevant for the given festival. Returns an associative array.
     *
     * @param Festival $festival the festival to search for
     * @return array all travel parties. See {@link $DTO_TRAVELPARTY} for the structure.
     */
    public function getTravelParties(Festival $festival): array {
        $parties = $this->travelPartyRepository->findAllWhereFestival($festival);
        return $this->mapper->mapToArrays($parties, self::$DTO_TRAVELPARTY);
    }

    /**
     * Saves a new guestlist entry to the database. Also publishes an update via mercure.
     *
     * @param Guestlist $guestlist the entry to save
     * @return array the new guestlist entry. See {@link $DTO_GUESTLIST} for the structure.
     */
    public function createGuestlist(Guestlist $guestlist): array {
        $this->doctrine->persist($guestlist);
        $this->doctrine->flush();

        $dto = $this->mapper->mapToArray($guestlist, self::$DTO_GUESTLIST);
        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $dto);
        return $dto;
    }

    /**
     * Saves a new band to the database.
     *
     * @param Band $band the band to save
     * @return array the new band. See {@link $DTO_BAND} for the structure.
     */
    public function createBand(Band $band): array {
        $this->doctrine->persist($band);
        $this->doctrine->flush();

        return $this->mapper->mapToArray($band, self::$DTO_BAND);
    }

    /**
     * Saves a new travel party to the database. Also publishes an update via mercure.
     *
     * @param TravelParty $party the party to save
     * @return array the new travel party. See {@link $DTO_TRAVELPARTY} for the structure.
     */
    public function createTravelParty(TravelParty $party): array {
        $this->doctrine->persist($party);
        $this->doctrine->flush();

        $dto = $this->mapper->mapToArray($party, self::$DTO_TRAVELPARTY);
        $this->mercure->publish('bandcheckin/parties/' . $party->getDay()->getFestival()->getId(), $dto);
        return $dto;
    }

    /**
     * Saves an existing guestlist entry to the database. Also publishes an update via mercure.
     *
     * @param Guestlist $guestlist the entry to save
     * @return array the modified guestlist entry. See {@link $DTO_GUESTLIST} for the structure.
     */
    public function saveGuestlist(Guestlist $guestlist): array {
        $this->doctrine->flush();

        $dto = $this->mapper->mapToArray($guestlist, self::$DTO_GUESTLIST);
        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $dto);
        return $dto;
    }

    /**
     * Saves an existing band to the database.
     *
     * @param Band $band the band to save
     * @return array the modified band. See {@link $DTO_BAND} for the structure.
     */
    public function saveBand(Band $band): array {
        $this->doctrine->flush();

        return $this->mapper->mapToArray($band, self::$DTO_BAND);
    }

    /**
     * Saves an existing travel party to the database.
     *
     * @param TravelParty $party the party to save
     * @return array the modified travel party. See {@link $DTO_TRAVELPARTY} for the structure.
     */
    public function saveTravelParty(TravelParty $party): array {
        $this->doctrine->flush();

        $dto = $this->mapper->mapToArray($party, self::$DTO_TRAVELPARTY);
        $this->mercure->publish('bandcheckin/parties/' . $party->getDay()->getFestival()->getId(), $dto);
        return $dto;
    }

    /**
     * Deletes an existing guestlist entry from the database. Also publishes an update via mercure.
     *
     * @param Guestlist $guestlist the entry to delete
     */
    public function deleteGuestlist(Guestlist $guestlist): void {
        $dto = [
            'id' => $guestlist->getId(),
            'delete' => true
        ];
        $this->doctrine->remove($guestlist);
        $this->doctrine->flush();

        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $dto);
    }

    /**
     * Deletes an existing band from the database.
     *
     * @param Band $band the band to delete
     */
    public function deleteBand(Band $band): void {
        $this->doctrine->remove($band);
        $this->doctrine->flush();
    }

    /**
     * Deletes an existing travel party from the database. Also publishes an update via mercure.
     *
     * @param TravelParty $party the entry to delete
     */
    public function deleteTravelParty(TravelParty $party): void {
        $dto = [
            'id' => $party->getId(),
            'delete' => true
        ];
        $this->doctrine->remove($party);
        $this->doctrine->flush();

        $this->mercure->publish('bandcheckin/parties/' . $party->getDay()->getFestival()->getId(), $dto);
    }

    /**
     * Sets the checkedIn property of an entry to true. Also publishes an update via mercure.
     *
     * @param Guestlist $guestlist the entry to check in
     */
    public function checkin(Guestlist $guestlist): void {
        $guestlist->setCheckedIn(true);
        $this->doctrine->flush();

        $dto = $this->mapper->mapToArray($guestlist, self::$DTO_GUESTLIST);
        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $dto);
    }

    /**
     * Sets the checkedIn and backstageCheckedIn property of an entry to true. Also publishes an update via mercure.
     *
     * @param Guestlist $guestlist the entry to check in
     */
    public function checkinBackstage(Guestlist $guestlist): void {
        $guestlist->setCheckedIn(true);
        $guestlist->setBackstageCheckedIn(true);
        $this->doctrine->flush();

        $dto = $this->mapper->mapToArray($guestlist, self::$DTO_GUESTLIST);
        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $dto);
    }

    /**
     * Sets the checkedIn property of a travel party to true. Also publishes an update via mercure.
     *
     * @param TravelParty $party the party to check in
     */
    public function checkinParty(TravelParty $party): void {
        $party->setCheckedIn(true);
        $this->doctrine->flush();

        $dto = $this->mapper->mapToArray($party, self::$DTO_TRAVELPARTY);
        $this->mercure->publish('bandcheckin/parties/' . $party->getDay()->getFestival()->getId(), $dto);
    }
}
