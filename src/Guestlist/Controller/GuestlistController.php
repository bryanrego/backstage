<?php

namespace App\Guestlist\Controller;

use App\Festival\Entity\Festival;
use App\Festival\Repository\FestivalDayRepository;
use App\System\Application\MercureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GuestlistController extends AbstractController {
    private FestivalDayRepository $festivalDayRepository;

    public function __construct(FestivalDayRepository $festivalDayRepository) {
        $this->festivalDayRepository = $festivalDayRepository;
    }

    /**
     * Display the guestlist page. Content gets loaded via javascript asynchronously.
     * Authorizes mercure subscriptions for live updates.
     */
    #[Route(path: '/guestlist/{festival<\d+>?}', name: 'app_guestlist')]
    public function index(Festival $festival, MercureService $mercure, Request $request): Response {
        $this->denyAccessUnlessGranted('guestlist.info');
        $days = $this->festivalDayRepository->findAllDuringFestival($festival);

        $mercure->authorizeSubscription($request, [
            'guestlist/' . $festival->getId()
        ]);

        return $this->render('guestlist/index.html.twig', [
            'festival' => $festival,
            'days' => $days
        ]);
    }
}
