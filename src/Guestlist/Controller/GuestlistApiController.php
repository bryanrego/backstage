<?php

namespace App\Guestlist\Controller;

use App\AbstractApiController;
use App\Festival\Application\FestivalService;
use App\Festival\Entity\Festival;
use App\Guestlist\Application\GuestlistService;
use App\Guestlist\Entity\Guestlist;
use App\Guestlist\Entity\GuestlistDay;
use App\System\Application\MercureService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Handles all API requests for the guestlist. As almost everything is done
 * through javascript this controller does most of the work.
 */
#[Route(path: '/api/guestlist')]
class GuestlistApiController extends AbstractApiController {
    private GuestlistService $guestlistService;
    private FestivalService $festivalService;

    public function __construct(GuestlistService $guestlistService, FestivalService $festivalService) {
        $this->guestlistService = $guestlistService;
        $this->festivalService = $festivalService;
    }

    /**
     * Finds all guestlist entries for a festival. Adds the mercure discovery
     * to the response to enable live updates on the client side.
     *
     * @param Festival $festival id of the festival
     */
    #[Route(path: '/{festival<\d+>}', methods: ['GET'])]
    public function getGuestlist(Festival $festival, MercureService $mercure, Request $request): Response {
        $this->denyAccessUnlessGranted('guestlist.info');

        $mercure->addDiscovery($request);
        $response = $this->responseOk($this->guestlistService->getGuestlist($festival));
        $mercure->finalizeResponse($response);
        return $response;
    }

    /**
     * Creates a new guestlist entry for the given festival.
     * See {@link validateGuestlistRequest()} for required request arguments.
     *
     * @param Festival $festival id of the festival
     */
    #[Route(path: '/{festival<\d+>}', methods: ['POST'])]
    public function postGuestlist(Festival $festival, Request $request): Response {
        $this->denyAccessUnlessGranted('guestlist.edit');

        $data = $this->validateGuestlistRequest($request);

        // set values
        $guestlist = new Guestlist();
        $guestlist->setFestival($festival);
        $guestlist->setName($data->name);
        $guestlist->setOrigin($data->origin);
        $guestlist->setComment($data->comment);
        $guestlist->setPlus($data->plus);
        $guestlist->setCamping($data->camping);
        $guestlist->setCheckedIn($data->checkedIn);
        $guestlist->setBackstageCheckedIn($data->backstageCheckedIn);

        // add days
        foreach ($data->days as $day) {
            $fd = $this->festivalService->getFestivalDay($day->id);
            $guestlistDay = new GuestlistDay();
            $guestlistDay->setDay($fd);
            $guestlistDay->setBackstage($day->backstage);
            $guestlist->addDay($guestlistDay);
        }

        $dto = $this->guestlistService->createGuestlist($guestlist);

        return $this->responseOk($dto);
    }

    /**
     * Edits an existing guestlist entry. See {@link validateGuestlistRequest()} for required request arguments.
     *
     * @param Guestlist $guestlist id of the guestlist entry
     */
    #[Route(path: '/edit/{guestlist<\d+>}', methods: ['PUT'])]
    public function putGuestlist(Guestlist $guestlist, Request $request): Response {
        $this->denyAccessUnlessGranted('guestlist.edit');

        $data = $this->validateGuestlistRequest($request);

        // update values
        $guestlist->setName($data->name);
        $guestlist->setOrigin($data->origin);
        $guestlist->setComment($data->comment);
        $guestlist->setPlus($data->plus);
        $guestlist->setCamping($data->camping);
        $guestlist->setCheckedIn($data->checkedIn);
        $guestlist->setBackstageCheckedIn($data->backstageCheckedIn);

        // update existing and add new days
        foreach ($data->days as $day) {
            $fd = $this->festivalService->getFestivalDay($day->id);
            $guestlistDay = $this->guestlistService->getGuestlistDay($guestlist, $fd);
            if ($guestlistDay == null) {
                $guestlistDay = new GuestlistDay();
                $guestlistDay->setDay($fd);
                $guestlist->addDay($guestlistDay);
            }
            $guestlistDay->setBackstage($day->backstage);
        }
        // remove old days
        foreach ($guestlist->getDays() as $guestlistDay) {
            $found = false;
            foreach ($data->days as $day) {
                if ($guestlistDay->getDay()->getId() === $day->id) {
                    $found = true;
                    break;
                }
            }
            if (!$found)
                $guestlist->removeDay($guestlistDay);
        }

        $dto = $this->guestlistService->saveGuestlist($guestlist);

        return $this->responseOk($dto);
    }

    /**
     * Validates a HTTP request for required guestlist arguments.
     */
    private function validateGuestlistRequest(Request $request) {
        $data = $this->resolveBody($request, [
            'name' => 'string',
            'origin' => 'string',
            'comment' => '?string',
            'plus' => '?int',
            'days' => 'array',
            'camping' => 'bool',
            'checkedIn' => 'bool',
            'backstageCheckedIn' => 'bool'
        ]);
        if ($data === false)
            throw new BadRequestHttpException('Required arguments missing');

        if (empty($data->days))
            throw new BadRequestHttpException('At least one day is required');

        foreach ($data->days as $day) {
            if (!property_exists($day, 'id') || !property_exists($day, 'backstage'))
                throw new BadRequestHttpException('Day arguments missing');
            if (!is_int($day->id) || !is_bool($day->backstage))
                throw new BadRequestHttpException('Invalid day arguments');

            $fd = $this->festivalService->getFestivalDay($day->id);
            if ($fd == null)
                throw new BadRequestHttpException('Unknown festival day ' . $day->id);
        }

        return $data;
    }

    /**
     * Deletes an existing guestlist entry.
     *
     * @param Guestlist $guestlist id of the guestlist entry
     */
    #[Route(path: '/delete/{guestlist<\d+>}', methods: ['DELETE'])]
    public function deleteGuestlist(Guestlist $guestlist): Response {
        $this->denyAccessUnlessGranted('guestlist.edit');

        $this->guestlistService->deleteGuestlist($guestlist);
        return $this->responseOk();
    }

    /**
     * Checks in a guestlist entry. Only valid if the entry is not already checked in.
     *
     * @param Guestlist $guestlist id of the guestlist entry
     */
    #[Route(path: '/checkin/{guestlist<\d+>}', methods: ['PUT'])]
    public function checkinGuestlist(Guestlist $guestlist): Response {
        $this->denyAccessUnlessGranted('guestlist.checkin');

        if ($guestlist->getCheckedIn())
            return $this->responseBadRequest('Already checked in');

        $this->guestlistService->checkin($guestlist);
        return $this->responseOk();
    }

    /**
     * Checks in a guestlist entry (backstage). Only valid if the entry is not already checked in (backstage).
     *
     * @param Guestlist $guestlist id of the guestlist entry
     */
    #[Route(path: '/checkin/backstage/{guestlist<\d+>}', methods: ['PUT'])]
    public function checkinBackstage(Guestlist $guestlist): Response {
        $this->denyAccessUnlessGranted('guestlist.backstage');

        if ($guestlist->getBackstageCheckedIn())
            return $this->responseBadRequest('Already checked in');

        $this->guestlistService->checkinBackstage($guestlist);
        return $this->responseOk();
    }
}
