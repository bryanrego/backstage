<?php

namespace App\Guestlist\Controller;

use App\AbstractApiController;
use App\Festival\Application\FestivalService;
use App\Festival\Entity\Festival;
use App\Guestlist\Application\GuestlistService;
use App\Guestlist\Entity\Band;
use App\Guestlist\Entity\TravelParty;
use App\System\Application\MercureService;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/bandcheckin')]
class BandCheckinApiController extends AbstractApiController {
    private GuestlistService $guestlistService;
    private FestivalService $festivalService;
    private KernelInterface $kernel;

    public function __construct(GuestlistService $guestlistService, FestivalService $festivalService, KernelInterface $kernel) {
        $this->guestlistService = $guestlistService;
        $this->festivalService = $festivalService;
        $this->kernel = $kernel;
    }

    #[Route(path: '/bands', methods: ['GET'])]
    public function getBands(): Response {
        $this->denyAccessUnlessGranted('checkin.bands');

        $bands = $this->guestlistService->getBands();

        return $this->responseOk($bands);
    }

    #[Route(path: '/bands', methods: ['POST'])]
    public function postBand(Request $request): Response {
        $this->denyAccessUnlessGranted('checkin.bands.edit');

        $data = $this->resolveBody($request, [
            'name' => 'string',
            'location' => '?string',
            'contactName' => '?string',
            'contactEmail' => '?string',
            'contactPhone' => '?string',
        ]);
        if ($data === false)
            throw new BadRequestHttpException('Required arguments missing or invalid');

        $band = new Band();
        $band->setName($data->name);
        $band->setLocation($data->location);
        $band->setContactName($data->contactName);
        $band->setContactEmail($data->contactEmail);
        $band->setContactPhone($data->contactPhone);
        $dto = $this->guestlistService->createBand($band);

        return $this->responseOk($dto);
    }

    #[Route(path: '/bands/edit/{band<\d+>}', methods: ['PUT'])]
    public function putBand(Band $band, Request $request): Response {
        $this->denyAccessUnlessGranted('checkin.bands.edit');

        $data = $this->resolveBody($request, [
            'name' => 'string',
            'location' => '?string',
            'contactName' => '?string',
            'contactEmail' => '?string',
            'contactPhone' => '?string',
        ]);
        if ($data === false)
            throw new BadRequestHttpException('Required arguments missing or invalid');

        $band->setName($data->name);
        $band->setLocation($data->location);
        $band->setContactName($data->contactName);
        $band->setContactEmail($data->contactEmail);
        $band->setContactPhone($data->contactPhone);
        $dto = $this->guestlistService->saveBand($band);

        return $this->responseOk($dto);
    }

    #[Route(path: '/bands/delete/{band<\d+>}', methods: ['DELETE'])]
    public function deleteBand(Band $band): Response {
        $this->denyAccessUnlessGranted('checkin.bands.edit');

        $this->guestlistService->deleteBand($band);

        return $this->responseOk();
    }

    #[Route(path: '/parties/{festival<\d+>}', methods: ['GET'])]
    public function getTravelParties(Festival $festival, MercureService $mercure, Request $request): Response {
        $this->denyAccessUnlessGranted('checkin.info');

        $mercure->addDiscovery($request);
        $response = $this->responseOk($this->guestlistService->getTravelParties($festival));
        $mercure->finalizeResponse($response);

        return $response;
    }

    #[Route(path: '/parties/{festival<\d+>}', methods: ['POST'])]
    public function postTravelParty(Festival $festival, Request $request): Response {
        $this->denyAccessUnlessGranted('checkin.edit');

        $party = $this->validateTravelPartyRequest($request, $festival);
        $dto = $this->guestlistService->createTravelParty($party);

        return $this->responseOk($dto);
    }

    #[Route(path: '/parties/edit/{party<\d+>}', methods: ['PUT'])]
    public function putTravelParty(TravelParty $party, Request $request): Response {
        $this->denyAccessUnlessGranted('checkin.edit');

        $party = $this->validateTravelPartyRequest($request, $party->getDay()->getFestival(), $party);
        $dto = $this->guestlistService->saveTravelParty($party);

        return $this->responseOk($dto);
    }

    #[Route(path: '/parties/delete/{party<\d+>}', methods: ['DELETE'])]
    public function deleteTravelParty(TravelParty $party): Response {
        $this->denyAccessUnlessGranted('checkin.edit');

        $this->guestlistService->deleteTravelParty($party);
        return $this->responseOk();
    }

    private function validateTravelPartyRequest(Request $request, Festival $festival, TravelParty $party = null): TravelParty {
        $data = $this->resolveBody($request, [
            'band' => 'int',
            'day' => 'int',
            'stageTime' => 'string',
            'vehicle' => '?string',
            'checkedIn' => 'bool',
            'guestlist' => 'array'
        ]);
        if ($data === false)
            throw new BadRequestHttpException('Required arguments missing or invalid');

        $band = $this->guestlistService->getBand($data->band);
        if ($band == null)
            throw new BadRequestHttpException('Unknown band');

        $day = $this->festivalService->getFestivalDay($data->day);
        if ($day == null)
            throw new BadRequestHttpException('Unknown day');

        if ($day->getFestival()->getId() !== $festival->getId())
            throw new BadRequestHttpException('Day does not belong to current festival');

        $guestlist = $this->guestlistService->getGuestlistById($data->guestlist);
        if (sizeof($guestlist) != sizeof($data->guestlist))
            throw new BadRequestHttpException('Could not find all specified guestlist entries');

        if ($party == null)
            $party = new TravelParty();
        $party->setBand($band);
        $party->setDay($day);
        // set timezone manually, because times are saved in local time in the database :(
        $stageTime = DateTime::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $data->stageTime);
        $stageTime->setTimezone(new DateTimeZone($this->kernel->getContainer()->getParameter('timezone')));
        $party->setStageTime($stageTime);
        $party->setVehicle($data->vehicle);
        $party->setCheckedIn($data->checkedIn);
        foreach ($party->getGuestlist() as $entry) {
            if (!in_array($entry, $guestlist))
                $party->removeGuestlist($entry);
        }
        foreach ($guestlist as $entry)
            $party->addGuestlist($entry);

        return $party;
    }

    #[Route(path: '/checkin/{party<\d+>}', methods: ['PUT'])]
    public function checkinParty(TravelParty $party): Response {
        $this->denyAccessUnlessGranted('checkin.checkin');

        $this->guestlistService->checkinParty($party);

        return $this->responseOk();
    }
}
