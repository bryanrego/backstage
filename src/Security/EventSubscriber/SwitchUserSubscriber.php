<?php

namespace App\Security\EventSubscriber;

use App\Security\Voter\PermissionVoter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * Clears the permission cache when switching users to force the permission voter to reload from the database.
 */
class SwitchUserSubscriber implements EventSubscriberInterface {
    public function onSwitchUser(SwitchUserEvent $event): void {
        $request = $event->getRequest();
        if ($request->hasSession())
            $request->getSession()->remove(PermissionVoter::SESSION_PERMISSION_CACHE);
    }

    public static function getSubscribedEvents(): array {
        return [
            SecurityEvents::SWITCH_USER => 'onSwitchUser'
        ];
    }
}
