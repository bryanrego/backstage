<?php

namespace App\Security\Controller;

use App\AbstractApiController;
use App\Security\Entity\Permission;
use App\Security\Entity\PermissionGroup;
use App\Security\Entity\PermissionInheritance;
use App\Security\Entity\User;
use App\Security\Repository\PermissionGroupRepository;
use App\Security\Repository\PermissionInheritanceRepository;
use App\Security\Repository\PermissionRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/admin')]
class ApiController extends AbstractApiController {

    #[Route(path: '/permissions/{group<\d+>}', methods: ['GET'])]
    public function getPermissions($group, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        /** @var PermissionRepository $repo */
        $repo = $doctrine->getRepository('\App\Security\Entity\Permission');
        $permissions = array_map(function ($p) {
            return $p['permission'];
        }, $repo->findPermissions($group));

        return $this->responseOk($permissions);
    }

    #[Route(path: '/permissions', methods: ['PUT'])]
    public function putPermission(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'group' => 'int',
            'permission' => 'string'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var PermissionGroupRepository $repo */
        $repo = $doctrine->getRepository('\App\Security\Entity\PermissionGroup');
        $group = $repo->find($data->group);
        if ($group === null)
            return $this->responseBadRequest();

        $permission = new Permission();
        $permission->setPermission($data->permission);
        $permission->setPermissionGroup($group);
        $doctrine->persist($permission);
        try {
            $doctrine->flush();
        } catch (UniqueConstraintViolationException $e) {
            return $this->responseBadRequest('Permission already granted');
        }

        return $this->responseOk();
    }

    #[Route(path: '/permissions', methods: ['DELETE'])]
    public function deletePermission(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'group' => 'int',
            'permission' => 'string'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var PermissionRepository $repo */
        $repo = $doctrine->getRepository('\App\Security\Entity\Permission');
        $permission = $repo->findOneBy([
            'permission' => $data->permission,
            'permissionGroup' => $data->group
        ]);
        if ($permission === null)
            return $this->responseBadRequest();

        $doctrine->remove($permission);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/usergroups/{user<\d+>}', methods: ['GET'])]
    public function getUserGroups($user, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.users'))
            return $this->responseForbidden();

        /** @var PermissionGroupRepository $groupRepo */
        $groupRepo = $doctrine->getRepository('\App\Security\Entity\PermissionGroup');
        /** @var PermissionInheritanceRepository $inheritanceRepo */
        $inheritanceRepo = $doctrine->getRepository('\App\Security\Entity\PermissionInheritance');
        $inheritances = [];
        foreach ($inheritanceRepo->findUserGroups($user) as $inheritance) {
            $inheritances[] = $inheritance->getPermissionGroup()->getId();
        }

        $groups = [];
        foreach ($groupRepo->findAll() as $group) {
            $groups[] = [
                'id' => $group->getId(),
                'name' => $group->getName(),
                'member' => array_search($group->getId(), $inheritances) !== false
            ];
        }
        return $this->responseOk($groups);
    }

    #[Route(path: '/usergroups', methods: ['PUT'])]
    public function putUserGroup(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.users'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'user' => 'int',
            'group' => 'int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var User $user */
        $user = $doctrine->find('\App\Security\Entity\User', $data->user);
        $group = $doctrine->find('\App\Security\Entity\PermissionGroup', $data->group);
        if ($user === null || $group === null)
            return $this->responseBadRequest();

        $inheritance = new PermissionInheritance();
        $inheritance->setUser($user);
        $inheritance->setPermissionGroup($group);
        $doctrine->persist($inheritance);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/usergroups', methods: ['DELETE'])]
    public function deleteUserGroup(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.users'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'user' => 'int',
            'group' => 'int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $inheritance = $doctrine->getRepository('\App\Security\Entity\PermissionInheritance')->findOneBy([
            'user' => $data->user,
            'permissionGroup' => $data->group
        ]);
        $doctrine->remove($inheritance);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/groups', methods: ['POST'])]
    public function postGroup(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'name' => 'string',
            'parent' => '?int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $group = new PermissionGroup();
        $group->setName($data->name);
        if (isset($data->parent)) {
            /** @var PermissionGroup $parent */
            $parent = $doctrine->find('\App\Security\Entity\PermissionGroup', $data->parent);
            if ($parent === null)
                return $this->responseBadRequest();
            $group->setParent($parent);
        }
        $doctrine->persist($group);
        $doctrine->flush();

        return $this->responseOk($group->getId());
    }

    #[Route(path: '/groups/{id<\d+>}', methods: ['PUT'])]
    public function putGroup(int $id, Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'name' => 'string',
            'parent' => '?int'
        ]);
        if ($data === false || $id === $data->parent)
            return $this->responseBadRequest();

        /** @var PermissionGroup $group */
        $group = $doctrine->find('\App\Security\Entity\PermissionGroup', $id);
        if ($group === null)
            return $this->responseBadRequest();

        $group->setName($data->name);
        if (!empty($data->parent)) {
            /** @var PermissionGroup $parent */
            $parent = $doctrine->find('\App\Security\Entity\PermissionGroup', $data->parent);
            if ($parent === null)
                return $this->responseBadRequest();
            $group->setParent($parent);
        } else {
            $group->setParent(null);
        }
        $doctrine->persist($group);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/groups/{id<\d+>}', methods: ['DELETE'])]
    public function delGroup($id, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        /** @var PermissionGroup $group */
        $group = $doctrine->find('\App\Security\Entity\PermissionGroup', $id);
        if ($group === null)
            return $this->responseBadRequest();

        $doctrine->remove($group);
        $doctrine->flush();

        return $this->responseOk();
    }

}
