<?php

namespace App\Security\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Security\Repository\PermissionInheritanceRepository")
 */
class PermissionInheritance {
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\User", inversedBy="permissionInheritances")
     * @ORM\JoinColumn(nullable=false)
     */
    private User $user;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\PermissionGroup", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private PermissionGroup $permissionGroup;

    public function getUser(): User {
        return $this->user;
    }

    public function setUser($user): self {
        $this->user = $user;

        return $this;
    }

    public function getPermissionGroup(): PermissionGroup {
        return $this->permissionGroup;
    }

    public function setPermissionGroup($permissionGroup): self {
        $this->permissionGroup = $permissionGroup;

        return $this;
    }
}
