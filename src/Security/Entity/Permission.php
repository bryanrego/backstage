<?php

namespace App\Security\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="App\Security\Repository\PermissionRepository")
 */
class Permission implements JsonSerializable {
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\PermissionGroup", inversedBy="permissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?PermissionGroup $permissionGroup = null;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private ?string $permission = null;

    public function getPermissionGroup(): ?PermissionGroup {
        return $this->permissionGroup;
    }

    public function setPermissionGroup(?PermissionGroup $permissionGroup): self {
        $this->permissionGroup = $permissionGroup;

        return $this;
    }

    public function getPermission(): ?string {
        return $this->permission;
    }

    public function setPermission(string $permission): self {
        $this->permission = $permission;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return array data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
