<?php

namespace App\Security\Entity;

use App\Band\Entity\ApplicationComment;
use App\Band\Entity\ApplicationFavorite;
use App\Band\Entity\ApplicationHistory;
use App\Band\Entity\ApplicationScore;
use App\Band\Entity\FinalScore;
use App\Inventory\Entity\Item;
use App\Inventory\Entity\Log;
use App\Volunteer\Entity\ShiftArea;
use App\Volunteer\Entity\Volunteer;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Security\Repository\UserRepository")
 * @UniqueEntity(
 *     fields={"email"},
 *     message="email.duplicate"
 * )
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email
     * @Assert\NotBlank
     */
    private ?string $email = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $name = null;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $activation_token = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $registered_at = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $activationEmailSentAt = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $activated_at = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $last_login = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $password_reset = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $password_reset_timeout = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $force_reset = null;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private ?string $lang = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Volunteer\Entity\Volunteer", mappedBy="user", cascade={"persist", "remove"})
     */
    private ?Volunteer $volunteer = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Volunteer\Entity\ShiftArea", mappedBy="supervisor")
     */
    private Collection $supervisingAreas;

    /**
     * @ORM\OneToMany(targetEntity="App\Security\Entity\PermissionInheritance", mappedBy="user")
     */
    private Collection $permissionInheritances;

    /**
     * @ORM\OneToMany(targetEntity="App\Band\Entity\ApplicationHistory", mappedBy="user", orphanRemoval=true)
     */
    private Collection $bandApplicationHistories;

    /**
     * @ORM\OneToMany(targetEntity="App\Band\Entity\ApplicationScore", mappedBy="user", orphanRemoval=true)
     */
    private Collection $bandApplicationScores;

    /**
     * @ORM\OneToMany(targetEntity="App\Band\Entity\ApplicationFavorite", mappedBy="user", orphanRemoval=true)
     */
    private Collection $bandApplicationFavorites;

    /**
     * @ORM\OneToMany(targetEntity="App\Band\Entity\ApplicationComment", mappedBy="author", orphanRemoval=true)
     */
    private Collection $bandApplicationComments;

    /**
     * @ORM\OneToMany(targetEntity="App\Band\Entity\FinalScore", mappedBy="user", orphanRemoval=true)
     */
    private Collection $bandFinalScores;

    /**
     * @ORM\OneToMany(targetEntity="App\Inventory\Entity\Item", mappedBy="borrowedBy")
     */
    private Collection $inventoryItems;

    public function __construct() {
        $this->supervisingAreas = new ArrayCollection();
        $this->permissionInheritances = new ArrayCollection();
        $this->bandApplicationHistories = new ArrayCollection();
        $this->bandApplicationScores = new ArrayCollection();
        $this->bandApplicationFavorites = new ArrayCollection();
        $this->bandApplicationComments = new ArrayCollection();
        $this->bandFinalScores = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getUserIdentifier(): string {
        return $this->email;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): ?string {
        // not needed when using the "bcrypt" algorithm in security.yaml
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials() {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(?string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getDisplayName(): string {
        if ($this->name != null)
            return $this->name;
        return $this->email;
    }

    public function getActivationToken(): ?string {
        return $this->activation_token;
    }

    public function setActivationToken(string $activation_token): self {
        $this->activation_token = $activation_token;

        return $this;
    }

    public function getRegisteredAt(): ?DateTimeInterface {
        return $this->registered_at;
    }

    public function setRegisteredAt(DateTimeInterface $registered_at): self {
        $this->registered_at = $registered_at;

        return $this;
    }

    public function getActivationEmailSentAt(): ?DateTimeInterface {
        return $this->activationEmailSentAt;
    }

    public function setActivationEmailSentAt(DateTimeInterface $activationEmailSentAt): self {
        $this->activationEmailSentAt = $activationEmailSentAt;

        return $this;
    }

    public function getActivatedAt(): ?DateTimeInterface {
        return $this->activated_at;
    }

    public function setActivatedAt(?DateTimeInterface $activated_at): self {
        $this->activated_at = $activated_at;

        return $this;
    }

    public function getLastLogin(): ?DateTimeInterface {
        return $this->last_login;
    }

    public function setLastLogin(DateTimeInterface $last_login): self {
        $this->last_login = $last_login;

        return $this;
    }

    public function getPasswordReset(): ?string {
        return $this->password_reset;
    }

    public function setPasswordReset(?string $password_reset): self {
        $this->password_reset = $password_reset;

        return $this;
    }

    public function getPasswordResetTimeout(): ?DateTimeInterface {
        return $this->password_reset_timeout;
    }

    public function setPasswordResetTimeout(?DateTimeInterface $password_reset_timeout): self {
        $this->password_reset_timeout = $password_reset_timeout;

        return $this;
    }

    public function getForceReset(): ?bool {
        return $this->force_reset;
    }

    public function setForceReset(bool $force_reset): self {
        $this->force_reset = $force_reset;

        return $this;
    }

    public function getLang(): ?string {
        return $this->lang;
    }

    public function setLang(string $lang): self {
        $this->lang = $lang;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array {
        return ['ROLE_USER'];
    }

    public function getVolunteer(): ?Volunteer {
        return $this->volunteer;
    }

    public function setVolunteer(Volunteer $volunteer): self {
        $this->volunteer = $volunteer;

        // set the owning side of the relation if necessary
        if ($this !== $volunteer->getUser()) {
            $volunteer->setUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|ShiftArea[]
     */
    public function getSupervisingAreas(): Collection {
        return $this->supervisingAreas;
    }

    public function addSupervisingArea(ShiftArea $supervisingArea): self {
        if (!$this->supervisingAreas->contains($supervisingArea)) {
            $this->supervisingAreas[] = $supervisingArea;
            $supervisingArea->setSupervisor($this);
        }

        return $this;
    }

    public function removeSupervisingArea(ShiftArea $supervisingArea): self {
        if ($this->supervisingAreas->contains($supervisingArea)) {
            $this->supervisingAreas->removeElement($supervisingArea);
            // set the owning side to null (unless already changed)
            if ($supervisingArea->getSupervisor() === $this) {
                $supervisingArea->setSupervisor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PermissionInheritance[]
     */
    public function getPermissionInheritances(): Collection {
        return $this->permissionInheritances;
    }

    public function addPermissionInheritance(PermissionInheritance $inheritance): self {
        if (!$this->permissionInheritances->contains($inheritance)) {
            $this->permissionInheritances[] = $inheritance;
            $inheritance->setUser($this);
        }

        return $this;
    }

    public function removePermission(PermissionInheritance $permission): self {
        $this->permissionInheritances->removeElement($permission);

        return $this;
    }

    /**
     * @return Collection|ApplicationHistory[]
     */
    public function getBandApplicationHistories(): Collection {
        return $this->bandApplicationHistories;
    }

    public function addBandApplicationHistory(ApplicationHistory $bandApplicationHistory): self {
        if (!$this->bandApplicationHistories->contains($bandApplicationHistory)) {
            $this->bandApplicationHistories[] = $bandApplicationHistory;
            $bandApplicationHistory->setUser($this);
        }

        return $this;
    }

    public function removeBandApplicationHistory(ApplicationHistory $bandApplicationHistory): self {
        if ($this->bandApplicationHistories->contains($bandApplicationHistory)) {
            $this->bandApplicationHistories->removeElement($bandApplicationHistory);
            // set the owning side to null (unless already changed)
            if ($bandApplicationHistory->getUser() === $this) {
                $bandApplicationHistory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationScore[]
     */
    public function getBandApplicationScores(): Collection {
        return $this->bandApplicationScores;
    }

    public function addBandApplicationScore(ApplicationScore $bandApplicationScore): self {
        if (!$this->bandApplicationScores->contains($bandApplicationScore)) {
            $this->bandApplicationScores[] = $bandApplicationScore;
            $bandApplicationScore->setUser($this);
        }

        return $this;
    }

    public function removeBandApplicationScore(ApplicationScore $bandApplicationScore): self {
        if ($this->bandApplicationScores->contains($bandApplicationScore)) {
            $this->bandApplicationScores->removeElement($bandApplicationScore);
            // set the owning side to null (unless already changed)
            if ($bandApplicationScore->getUser() === $this) {
                $bandApplicationScore->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationFavorite[]
     */
    public function getBandApplicationFavorites(): Collection {
        return $this->bandApplicationFavorites;
    }

    public function addBandApplicationFavorite(ApplicationFavorite $bandApplicationFavorite): self {
        if (!$this->bandApplicationFavorites->contains($bandApplicationFavorite)) {
            $this->bandApplicationFavorites[] = $bandApplicationFavorite;
            $bandApplicationFavorite->setUser($this);
        }

        return $this;
    }

    public function removeBandApplicationFavorite(ApplicationFavorite $bandApplicationFavorite): self {
        if ($this->bandApplicationFavorites->contains($bandApplicationFavorite)) {
            $this->bandApplicationFavorites->removeElement($bandApplicationFavorite);
            // set the owning side to null (unless already changed)
            if ($bandApplicationFavorite->getUser() === $this) {
                $bandApplicationFavorite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationComment[]
     */
    public function getBandApplicationComments(): Collection {
        return $this->bandApplicationComments;
    }

    public function addBandApplicationComment(ApplicationComment $bandApplicationComment): self {
        if (!$this->bandApplicationComments->contains($bandApplicationComment)) {
            $this->bandApplicationComments[] = $bandApplicationComment;
            $bandApplicationComment->setAuthor($this);
        }

        return $this;
    }

    public function removeBandApplicationComment(ApplicationComment $bandApplicationComment): self {
        if ($this->bandApplicationComments->contains($bandApplicationComment)) {
            $this->bandApplicationComments->removeElement($bandApplicationComment);
            // set the owning side to null (unless already changed)
            if ($bandApplicationComment->getAuthor() === $this) {
                $bandApplicationComment->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FinalScore[]
     */
    public function getBandFinalScores(): Collection {
        return $this->bandFinalScores;
    }

    public function addScore(FinalScore $score): self {
        if (!$this->bandFinalScores->contains($score)) {
            $this->bandFinalScores[] = $score;
            $score->setUser($this);
        }

        return $this;
    }

    public function removeScore(FinalScore $score): self {
        if ($this->bandFinalScores->contains($score)) {
            $this->bandFinalScores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getUser() === $this) {
                $score->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getInventoryItems(): Collection {
        return $this->inventoryItems;
    }

    public function addInventoryItem(Item $inventoryItem): self {
        if (!$this->inventoryItems->contains($inventoryItem)) {
            $this->inventoryItems[] = $inventoryItem;
            $inventoryItem->setBorrowedBy($this);
        }

        return $this;
    }

    public function removeInventoryItem(Item $inventoryItem): self {
        if ($this->inventoryItems->contains($inventoryItem)) {
            $this->inventoryItems->removeElement($inventoryItem);
            // set the owning side to null (unless already changed)
            if ($inventoryItem->getBorrowedBy() === $this) {
                $inventoryItem->setBorrowedBy(null);
            }
        }

        return $this;
    }
}
