<?php

namespace App\Security\Repository;

use App\Security\Entity\Permission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Permission|null find($id, $lockMode = null, $lockVersion = null)
 * @method Permission|null findOneBy(array $criteria, array $orderBy = null)
 * @method Permission[]    findAll()
 * @method Permission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermissionRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Permission::class);
    }

    public function findPermissions(int $group) {
        return $this->createQueryBuilder('p')
            ->select('p.permission')
            ->where('p.permissionGroup = :group')
            ->setParameter('group', $group)
            ->getQuery()
            ->getResult();
    }
}
