<?php

namespace App\Security\Repository;

use App\Security\Entity\User;
use App\System\Application\ConfigService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, User::class);
    }

    public function findCount($where = '', $parameters = []) {
        $query = $this->createQueryBuilder('u')
            ->select('COUNT(u.id)');
        if (!empty($where))
            $query->where($where)->setParameters($parameters);
        return $query->getQuery()->getSingleScalarResult();
    }

    public function findPage($page, $where = '', $parameters = []) {
        $query = $this->createQueryBuilder('u')
            ->select('u.id', 'u.email', ' u.name', ' u.registered_at', ' u.activated_at', ' u.last_login', ' u.force_reset', ' u.lang');
        if (!empty($where))
            $query->where($where)->setParameters($parameters);
        return $query
            ->orderBy('u.email')
            ->setFirstResult(($page - 1) * ConfigService::PAGE_SIZE)
            ->setMaxResults(ConfigService::PAGE_SIZE)
            ->getQuery()
            ->getResult();
    }
}
