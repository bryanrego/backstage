<?php

namespace App\Security\Exception;

use Exception;
use Throwable;

class DuplicateUserException extends Exception {
    public function __construct(?Throwable $previous = null) {
        parent::__construct('There already exists a user with this e-mail address', 0, $previous);
    }
}
