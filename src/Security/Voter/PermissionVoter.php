<?php

namespace App\Security\Voter;

use App\Security\Entity\PermissionGroup;
use App\Security\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Voter for permissions. Permissions are defined as lowercase words (a-z) and underscores (_) separated by period.
 */
class PermissionVoter extends Voter implements CacheableVoterInterface {
    public const SESSION_PERMISSION_CACHE = 'permission_cache';
    public const SESSION_PERMISSION_CACHE_TIMEOUT = 'permission_cache_timeout';

    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack) {
        $this->requestStack = $requestStack;
    }

    protected function supports(string $attribute, $subject): bool {
        return preg_match('/^[a-z_]+(\.[a-z_]+)*$/', $attribute);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool {
        $user = $token->getUser();
        // if the user is anonymous do not grant access
        if (!$user instanceof User)
            return false;

        // check if permissions are cached
        $session = $this->requestStack->getSession();
        if (!$session->has(self::SESSION_PERMISSION_CACHE) || $session->get(self::SESSION_PERMISSION_CACHE_TIMEOUT, 0) < time()) {
            $permissions = [];
            foreach ($user->getPermissionInheritances() as $group)
                $this->collectPermissions($group->getPermissionGroup(), $permissions);
            $permissions = array_unique($permissions);
            $session->set(self::SESSION_PERMISSION_CACHE, $permissions);
            $session->set(self::SESSION_PERMISSION_CACHE_TIMEOUT, time() + 180);  // cache for three minutes
        } else {
            $permissions = $session->get(self::SESSION_PERMISSION_CACHE);
        }

        // search for the exact permission string
        if (in_array($attribute, $permissions))
            return true;

        // check for global wildcard
        if (in_array('*', $permissions))
            return true;

        // search for wildcard .* permissions
        $parts = explode('.', $attribute);
        while (sizeof($parts) > 1) {
            // remove last element of array
            array_pop($parts);
            // append .* and search
            if (in_array(implode('.', $parts) . '.*', $permissions))
                return true;
        }
        return false;
    }

    private function collectPermissions(PermissionGroup $group, array &$permissions) {
        if ($group->getParent() !== null)
            $this->collectPermissions($group->getParent(), $permissions);
        foreach ($group->getPermissions() as $permission)
            $permissions[] = $permission->getPermission();
    }
}
