<?php

namespace App\Security\Application;

use App\Security\Entity\User;
use App\Security\Exception\DuplicateUserException;
use App\System\Application\MailService;
use App\System\Application\TokenGeneratorService;
use DateTimeImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AccountService {
    private UserPasswordHasherInterface $passwordHasher;
    private TokenGeneratorService $tokenGenerator;
    private RequestStack $requestStack;
    private EntityManagerInterface $doctrine;
    private MailService $mailService;

    public function __construct(UserPasswordHasherInterface $passwordHasher, TokenGeneratorService $tokenGenerator, RequestStack $requestStack, EntityManagerInterface $doctrine, MailService $mailService) {
        $this->passwordHasher = $passwordHasher;
        $this->tokenGenerator = $tokenGenerator;
        $this->requestStack = $requestStack;
        $this->doctrine = $doctrine;
        $this->mailService = $mailService;
    }

    /**
     * Creates a new user account.
     *
     * @param string $email the email address for the new user
     * @param string $password the password to set.
     * @return User the new user account
     * @throws DuplicateUserException if there exists another user with the given email address
     */
    public function createAccount(string $email, ?string $password = null): User {
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($this->passwordHasher->hashPassword($user, $password));
        $user->setActivationToken($this->tokenGenerator->generateToken(16));
        $now = new DateTimeImmutable();
        $user->setRegisteredAt($now);
        $user->setActivationEmailSentAt($now);
        $user->setLastLogin($user->getRegisteredAt());
        $user->setForceReset(false);
        $user->setLang($this->requestStack->getSession()->get('_locale', 'de'));

        try {
            $this->doctrine->persist($user);
            $this->doctrine->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new DuplicateUserException($e);
        }

        $this->mailService->sendRegisterEmail($user);

        return $user;
    }
}
