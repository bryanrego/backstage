<?php

namespace App\Band\Repository;

use App\Band\Entity\ApplicationFavorite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApplicationFavorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApplicationFavorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApplicationFavorite[]    findAll()
 * @method ApplicationFavorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationFavoriteRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ApplicationFavorite::class);
    }

    // /**
    //  * @return ApplicationFavorite[] Returns an array of ApplicationFavorite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApplicationFavorite
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
