<?php

namespace App\Band\Repository;

use App\Band\Entity\ApplicationComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApplicationComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApplicationComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApplicationComment[]    findAll()
 * @method ApplicationComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationCommentRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ApplicationComment::class);
    }

    public function findByApplication(int $id) {
        return $this->createQueryBuilder('c')
            ->select('c.id', 'u.id AS author_id', 'u.name AS author_name', 'u.email AS author_email', 'c.comment', 'c.createdAt', 'c.editedAt')
            ->join('c.author', 'u')
            ->where('c.application = :application')
            ->orderBy('c.createdAt')
            ->setParameter('application', $id)
            ->getQuery()
            ->getResult();
    }
}
