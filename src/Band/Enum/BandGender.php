<?php

namespace App\Band\Enum;

class BandGender {
    const FEMALE = 'female';
    const MALE = 'male';
    const NONBINARY = 'nonbinary';
    const MIXED = 'mixed';

    public static function getAvailableTypes(): array {
        return [
            self::FEMALE,
            self::MALE,
            self::NONBINARY,
            self::MIXED
        ];
    }
}