<?php

namespace App\Band\Enum;

class BandFinalsAction {
    const OPENDOOR = "opendoor";
    const AFTERSHOW = "aftershow";
    const BUDGET = "budget";

    public static function getAvailableTypes(): array {
        return [
            self::OPENDOOR,
            self::AFTERSHOW,
            self::BUDGET
        ];
    }
}
