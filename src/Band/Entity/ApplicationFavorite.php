<?php

namespace App\Band\Entity;

use App\Security\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Band\Repository\ApplicationFavoriteRepository")
 * @ORM\Table(name="band_application_favorite")
 */
class ApplicationFavorite {
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Band\Entity\Application", inversedBy="favorites")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Application $application;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\User", inversedBy="bandApplicationFavorites")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user;

    public function getApplication(): ?Application {
        return $this->application;
    }

    public function setApplication(?Application $application): self {
        $this->application = $application;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }
}
