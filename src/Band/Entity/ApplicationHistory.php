<?php

namespace App\Band\Entity;

use App\Security\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Band\Repository\ApplicationHistoryRepository")
 * @ORM\Table(name="band_application_history")
 */
class ApplicationHistory {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\User", inversedBy="bandApplicationHistories")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Band\Entity\Application", mappedBy="history", orphanRemoval=true)
     * @ORM\OrderBy({"festival" = "DESC"})
     */
    private Collection $applications;

    public function __construct() {
        $this->applications = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Application[]
     */
    public function getApplications(): Collection {
        return $this->applications;
    }

    public function addApplication(Application $application): self {
        if (!$this->applications->contains($application)) {
            $this->applications[] = $application;
            $application->setHistory($this);
        }

        return $this;
    }

    public function removeApplication(Application $application): self {
        if ($this->applications->contains($application)) {
            $this->applications->removeElement($application);
            // set the owning side to null (unless already changed)
            if ($application->getHistory() === $this) {
                $application->setHistory(null);
            }
        }

        return $this;
    }

    public function getFirstApplication(): Application {
        return $this->applications->first();
    }
}
