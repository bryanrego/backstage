<?php

namespace App\Band\Entity;

use App\Security\Entity\User;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Band\Repository\ApplicationCommentRepository")
 * @ORM\Table(name="band_application_comment")
 */
class ApplicationComment {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Band\Entity\Application", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Application $application = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\User", inversedBy="bandApplicationComments")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $author = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $comment = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $editedAt = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getApplication(): ?Application {
        return $this->application;
    }

    public function setApplication(?Application $application): self {
        $this->application = $application;

        return $this;
    }

    public function getAuthor(): ?User {
        return $this->author;
    }

    public function setAuthor(?User $author): self {
        $this->author = $author;

        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(string $comment): self {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getEditedAt(): ?DateTimeInterface {
        return $this->editedAt;
    }

    public function setEditedAt(?DateTimeInterface $editedAt): self {
        $this->editedAt = $editedAt;

        return $this;
    }
}
