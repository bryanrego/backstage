<?php

namespace App\Band\Entity;

use App\Security\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Band\Repository\ApplicationScoreRepository")
 * @ORM\Table(name="band_application_score")
 */
class ApplicationScore {
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Band\Entity\Application", inversedBy="scores")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Application $application;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Security\Entity\User", inversedBy="bandApplicationScores")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $score;

    public function getApplication(): ?Application {
        return $this->application;
    }

    public function setApplication(?Application $application): self {
        $this->application = $application;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getScore(): ?int {
        return $this->score;
    }

    public function setScore(int $score): self {
        $this->score = $score;

        return $this;
    }
}
