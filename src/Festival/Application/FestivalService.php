<?php

namespace App\Festival\Application;

use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use App\Festival\Repository\FestivalDayRepository;
use App\Festival\Repository\FestivalRepository;

class FestivalService {

    private FestivalRepository $festivalRepository;
    private FestivalDayRepository $festivalDayRepository;

    public function __construct(FestivalRepository $festivalRepository, FestivalDayRepository $festivalDayRepository) {
        $this->festivalRepository = $festivalRepository;
        $this->festivalDayRepository = $festivalDayRepository;
    }

    public function getFestival(int $id): ?Festival {
        return $this->festivalRepository->find($id);
    }

    public function getFestivalDay(int $id): ?FestivalDay {
        return $this->festivalDayRepository->find($id);
    }

    public function getOtherFestivals(Festival $except): array {
        return $this->festivalRepository->findOtherFestivals($except);
    }

}
