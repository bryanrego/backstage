<?php

namespace App\Festival\Entity;

use App\Band\Entity\Application;
use App\Band\Enum\BandFinalsStatus;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Festival\Repository\FestivalRepository")
 */
class Festival {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $start = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $end = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $number = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Festival\Entity\FestivalDay", mappedBy="festival", orphanRemoval=true)
     */
    private Collection $days;

    /**
     * @ORM\OneToMany(targetEntity="App\Band\Entity\Application", mappedBy="festival", orphanRemoval=true)
     */
    private Collection $bandApplications;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $bandApplicationStart = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $bandApplicationEnd = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $bandRatingEnd = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private ?string $bandFinalStatus = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Band\Entity\Application")
     */
    private ?Application $bandFinalCurrentVote = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $volunteerApplicationStart = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $volunteerApplicationEnd = null;

    public function __construct() {
        $this->days = new ArrayCollection();
        $this->bandApplications = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getStart(): ?DateTimeInterface {
        return $this->start;
    }

    public function setStart(DateTimeInterface $start): self {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?DateTimeInterface {
        return $this->end;
    }

    public function setEnd(DateTimeInterface $end): self {
        $this->end = $end;

        return $this;
    }

    public function getNumber(): ?int {
        return $this->number;
    }

    public function setNumber(int $number): self {
        $this->number = $number;

        return $this;
    }

    /**
     * @return Collection|FestivalDay[]
     */
    public function getDays(): Collection {
        return $this->days;
    }

    public function addDay(FestivalDay $day): self {
        if (!$this->days->contains($day)) {
            $this->days[] = $day;
            $day->setFestival($this);
        }

        return $this;
    }

    public function removeDay(FestivalDay $day): self {
        if ($this->days->contains($day)) {
            $this->days->removeElement($day);
            // set the owning side to null (unless already changed)
            if ($day->getFestival() === $this) {
                $day->setFestival(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Application[]
     */
    public function getBandApplications(): Collection {
        return $this->bandApplications;
    }

    public function addBandApplication(Application $bandApplication): self {
        if (!$this->bandApplications->contains($bandApplication)) {
            $this->bandApplications[] = $bandApplication;
            $bandApplication->setFestival($this);
        }

        return $this;
    }

    public function removeBandApplication(Application $bandApplication): self {
        if ($this->bandApplications->contains($bandApplication)) {
            $this->bandApplications->removeElement($bandApplication);
            // set the owning side to null (unless already changed)
            if ($bandApplication->getFestival() === $this) {
                $bandApplication->setFestival(null);
            }
        }

        return $this;
    }

    public function getBandApplicationStart(): ?DateTimeInterface {
        return $this->bandApplicationStart;
    }

    public function setBandApplicationStart(DateTimeInterface $bandApplicationStart): self {
        $this->bandApplicationStart = $bandApplicationStart;

        return $this;
    }

    public function getBandApplicationEnd(): ?DateTimeInterface {
        return $this->bandApplicationEnd;
    }

    public function setBandApplicationEnd(DateTimeInterface $bandApplicationEnd): self {
        $this->bandApplicationEnd = $bandApplicationEnd;

        return $this;
    }

    public function getBandRatingEnd(): ?DateTimeInterface {
        return $this->bandRatingEnd;
    }

    public function setBandRatingEnd(DateTimeInterface $bandRatingEnd): self {
        $this->bandRatingEnd = $bandRatingEnd;

        return $this;
    }

    public function getBandFinalStatus(): ?string {
        return $this->bandFinalStatus;
    }

    public function setBandFinalStatus(string $bandFinalStatus): self {
        if (!in_array($bandFinalStatus, BandFinalsStatus::getAvailableTypes()))
            throw new InvalidArgumentException("Invalid status");
        $this->bandFinalStatus = $bandFinalStatus;

        return $this;
    }

    public function getBandFinalCurrentVote(): ?Application {
        return $this->bandFinalCurrentVote;
    }

    public function setBandFinalCurrentVote(?Application $bandFinalCurrentVote): self {
        $this->bandFinalCurrentVote = $bandFinalCurrentVote;

        return $this;
    }

    public function getVolunteerApplicationStart(): ?DateTimeInterface {
        return $this->volunteerApplicationStart;
    }

    public function setVolunteerApplicationStart(DateTimeInterface $volunteerApplicationStart): self {
        $this->volunteerApplicationStart = $volunteerApplicationStart;

        return $this;
    }

    public function getVolunteerApplicationEnd(): ?DateTimeInterface {
        return $this->volunteerApplicationEnd;
    }

    public function setVolunteerApplicationEnd(DateTimeInterface $volunteerApplicationEnd): self {
        $this->volunteerApplicationEnd = $volunteerApplicationEnd;

        return $this;
    }
}
