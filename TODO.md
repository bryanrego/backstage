## Redesign
* Split controllers into more logical classes
* Use PHP8.0 annotations for routes
* document route functions
* Directly use Entities as route parameters
* Use annotations for security (permissions)
* Move all SQL and DQL from controllers to repositories, use services for each topic
* Sort translations by topic
* Rethink permissions and roles/groups
* Clean up javascript, drop jQuery, upgrade to Bootstrap 5
* Split SCSS into multiple files
* Use Messenger component to send emails asynchronously
