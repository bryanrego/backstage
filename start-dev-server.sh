#!/bin/bash

usage() {
  echo "Usage: $0 [-m]

Starts the dev server. Starts the mysql service if necessary. Specify the -m flag to also start mercure (and run in --no-tls mode)."
  exit 1
}

if [ "$(whoami)" == 'root' ]; then
  echo "Don't run this as root!"
  exit 1
fi

MERCURE='false'

while getopts "m" o; do
  case "${o}" in
  m)
    MERCURE='true'
    ;;
  *)
    usage
    ;;
  esac
done

systemctl is-active --quiet mysql || systemctl start mysql

if [ $MERCURE == 'true' ]; then
  systemctl is-active --quiet mercure || systemctl start mercure
  symfony server:start -d --no-tls
else
  symfony server:start -d
fi

symfony run -d yarn encore dev-server
