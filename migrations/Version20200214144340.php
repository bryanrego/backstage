<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200214144340 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE band_final_score (application_id INT NOT NULL, user_id INT NOT NULL, score INT NOT NULL, INDEX IDX_367A078B3E030ACD (application_id), INDEX IDX_367A078BA76ED395 (user_id), PRIMARY KEY(application_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE band_final_score ADD CONSTRAINT FK_367A078B3E030ACD FOREIGN KEY (application_id) REFERENCES band_application (id)');
        $this->addSql('ALTER TABLE band_final_score ADD CONSTRAINT FK_367A078BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE festival ADD band_final_current_vote_id INT DEFAULT NULL, ADD band_final_status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE festival ADD CONSTRAINT FK_57CF78947C386BA FOREIGN KEY (band_final_current_vote_id) REFERENCES band_application (id)');
        $this->addSql('CREATE INDEX IDX_57CF78947C386BA ON festival (band_final_current_vote_id)');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE band_final_score');
        $this->addSql('ALTER TABLE festival DROP FOREIGN KEY FK_57CF78947C386BA');
        $this->addSql('DROP INDEX IDX_57CF78947C386BA ON festival');
        $this->addSql('ALTER TABLE festival DROP band_final_current_vote_id, DROP band_final_status');
    }
}
