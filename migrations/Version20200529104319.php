<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200529104319 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE inventory_item_category (inventory_item_id INT NOT NULL, inventory_category_id INT NOT NULL, INDEX IDX_94C12FE536BF4A2 (inventory_item_id), INDEX IDX_94C12FE66B4A12D (inventory_category_id), PRIMARY KEY(inventory_item_id, inventory_category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE inventory_item_category ADD CONSTRAINT FK_94C12FE536BF4A2 FOREIGN KEY (inventory_item_id) REFERENCES inventory_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE inventory_item_category ADD CONSTRAINT FK_94C12FE66B4A12D FOREIGN KEY (inventory_category_id) REFERENCES inventory_category (id) ON DELETE CASCADE');
        $this->addSql('INSERT INTO inventory_item_category (inventory_item_id, inventory_category_id) SELECT id, category_id FROM inventory_item');
        $this->addSql('ALTER TABLE inventory_item DROP FOREIGN KEY FK_55BDEA3012469DE2');
        $this->addSql('DROP INDEX IDX_55BDEA3012469DE2 ON inventory_item');
        $this->addSql('ALTER TABLE inventory_item DROP category_id');
        $this->addSql('ALTER TABLE inventory_item ADD owner VARCHAR(255) NOT NULL');

    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inventory_item ADD category_id INT NOT NULL');
        $this->addSql('ALTER TABLE inventory_item DROP owner');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT FK_55BDEA3012469DE2 FOREIGN KEY (category_id) REFERENCES inventory_category (id)');
        $this->addSql('CREATE INDEX IDX_55BDEA3012469DE2 ON inventory_item (category_id)');
        $this->addSql('UPDATE inventory_item i JOIN inventory_item_category c ON i.id = c.inventory_item_id SET i.category_id = c.inventory_category_id');
        $this->addSql('DROP TABLE inventory_item_category');
    }
}
