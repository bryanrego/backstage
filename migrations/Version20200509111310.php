<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200509111310 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE inventory_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory_item (id INT AUTO_INCREMENT NOT NULL, borrowed_id INT DEFAULT NULL, category_id INT NOT NULL, location_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, count INT NOT NULL, missing TINYINT(1) NOT NULL, defective TINYINT(1) NOT NULL, deleted TINYINT(1) NOT NULL, INDEX IDX_55BDEA3064BC3968 (borrowed_id), INDEX IDX_55BDEA3012469DE2 (category_id), INDEX IDX_55BDEA3064D218E (location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory_item_image (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, image_name VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, INDEX IDX_AC744A0C126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory_location (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory_log (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, user_id INT NOT NULL, action VARCHAR(255) NOT NULL, timestamp DATETIME NOT NULL, old_value VARCHAR(255) DEFAULT NULL, new_value VARCHAR(255) DEFAULT NULL, INDEX IDX_F65507A1126F525E (item_id), INDEX IDX_F65507A1A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT FK_55BDEA3064BC3968 FOREIGN KEY (borrowed_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT FK_55BDEA3012469DE2 FOREIGN KEY (category_id) REFERENCES inventory_category (id)');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT FK_55BDEA3064D218E FOREIGN KEY (location_id) REFERENCES inventory_location (id)');
        $this->addSql('ALTER TABLE inventory_item_image ADD CONSTRAINT FK_AC744A0C126F525E FOREIGN KEY (item_id) REFERENCES inventory_item (id)');
        $this->addSql('ALTER TABLE inventory_log ADD CONSTRAINT FK_F65507A1126F525E FOREIGN KEY (item_id) REFERENCES inventory_item (id)');
        $this->addSql('ALTER TABLE inventory_log ADD CONSTRAINT FK_F65507A1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inventory_item DROP FOREIGN KEY FK_55BDEA3012469DE2');
        $this->addSql('ALTER TABLE inventory_log DROP FOREIGN KEY FK_F65507A1126F525E');
        $this->addSql('ALTER TABLE inventory_item DROP FOREIGN KEY FK_55BDEA3064D218E');
        $this->addSql('DROP TABLE inventory_item_image');
        $this->addSql('DROP TABLE inventory_category');
        $this->addSql('DROP TABLE inventory_item');
        $this->addSql('DROP TABLE inventory_location');
        $this->addSql('DROP TABLE inventory_log');
    }
}
