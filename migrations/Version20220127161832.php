<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220127161832 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE volunteer_days (volunteer_id INT NOT NULL, festival_day_id INT NOT NULL, INDEX IDX_5D55ED4A8EFAB6B1 (volunteer_id), INDEX IDX_5D55ED4AEE14DDD9 (festival_day_id), PRIMARY KEY(volunteer_id, festival_day_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE volunteer_preferred_areas (volunteer_id INT NOT NULL, shift_area_id INT NOT NULL, INDEX IDX_8C834FF88EFAB6B1 (volunteer_id), INDEX IDX_8C834FF8D05E7CE5 (shift_area_id), PRIMARY KEY(volunteer_id, shift_area_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE volunteer_days ADD CONSTRAINT FK_5D55ED4A8EFAB6B1 FOREIGN KEY (volunteer_id) REFERENCES volunteer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE volunteer_days ADD CONSTRAINT FK_5D55ED4AEE14DDD9 FOREIGN KEY (festival_day_id) REFERENCES festival_day (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE volunteer_preferred_areas ADD CONSTRAINT FK_8C834FF88EFAB6B1 FOREIGN KEY (volunteer_id) REFERENCES volunteer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE volunteer_preferred_areas ADD CONSTRAINT FK_8C834FF8D05E7CE5 FOREIGN KEY (shift_area_id) REFERENCES shift_area (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE volunteer DROP preferred_areas, DROP days');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE volunteer_days');
        $this->addSql('DROP TABLE volunteer_preferred_areas');
        $this->addSql('ALTER TABLE volunteer ADD preferred_areas LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:simple_array)\', ADD days LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:simple_array)\'');
    }
}
