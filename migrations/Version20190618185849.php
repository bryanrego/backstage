<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190618185849 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE volunteer_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_bin, color VARCHAR(255) NOT NULL COLLATE utf8_bin, UNIQUE INDEX UNIQ_5FAE99375E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE volunteer_group_volunteer (volunteer_group_id INT NOT NULL, volunteer_id INT NOT NULL, INDEX IDX_9A9F7E02D68889C6 (volunteer_group_id), INDEX IDX_9A9F7E028EFAB6B1 (volunteer_id), PRIMARY KEY(volunteer_group_id, volunteer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE volunteer_group_volunteer ADD CONSTRAINT FK_9A9F7E02D68889C6 FOREIGN KEY (volunteer_group_id) REFERENCES volunteer_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE volunteer_group_volunteer ADD CONSTRAINT FK_9A9F7E028EFAB6B1 FOREIGN KEY (volunteer_id) REFERENCES volunteer (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE volunteer_group_volunteer DROP FOREIGN KEY FK_9A9F7E02D68889C6');
        $this->addSql('DROP TABLE volunteer_group');
        $this->addSql('DROP TABLE volunteer_group_volunteer');
    }
}
