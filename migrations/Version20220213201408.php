<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220213201408 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        $this->addSql('ALTER TABLE volunteer ADD street VARCHAR(255) NOT NULL DEFAULT "", ADD zip VARCHAR(255) NOT NULL DEFAULT "", ADD city VARCHAR(255) NOT NULL DEFAULT ""');
    }

    public function down(Schema $schema): void {
        $this->addSql('ALTER TABLE volunteer DROP street, DROP zip, DROP city');
    }
}
