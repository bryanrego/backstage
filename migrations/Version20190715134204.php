<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190715134204 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE band (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, contact_name VARCHAR(255) NOT NULL, contact_email VARCHAR(255) NOT NULL, contact_phone VARCHAR(255) DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE band_member (id INT AUTO_INCREMENT NOT NULL, travel_party_id INT NOT NULL, name VARCHAR(255) NOT NULL, comment VARCHAR(255) DEFAULT NULL, arrived TINYINT(1) NOT NULL, INDEX IDX_89A1C7A94B4D731C (travel_party_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE travel_party (id INT AUTO_INCREMENT NOT NULL, band_id INT NOT NULL, day_id INT NOT NULL, vehicle VARCHAR(255) DEFAULT NULL, start_time DATETIME NOT NULL, UNIQUE INDEX UNIQ_2139C95649ABEB17 (band_id), INDEX IDX_2139C9569C24126 (day_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE band_member ADD CONSTRAINT FK_89A1C7A94B4D731C FOREIGN KEY (travel_party_id) REFERENCES travel_party (id)');
        $this->addSql('ALTER TABLE travel_party ADD CONSTRAINT FK_2139C95649ABEB17 FOREIGN KEY (band_id) REFERENCES band (id)');
        $this->addSql('ALTER TABLE travel_party ADD CONSTRAINT FK_2139C9569C24126 FOREIGN KEY (day_id) REFERENCES festival_day (id)');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE travel_party DROP FOREIGN KEY FK_2139C95649ABEB17');
        $this->addSql('ALTER TABLE band_member DROP FOREIGN KEY FK_89A1C7A94B4D731C');
        $this->addSql('DROP TABLE band');
        $this->addSql('DROP TABLE band_member');
        $this->addSql('DROP TABLE travel_party');
    }
}
