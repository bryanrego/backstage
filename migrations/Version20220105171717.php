<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220105171717 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE band_final_score ADD opendoor TINYINT(1) NOT NULL DEFAULT 0, ADD aftershow TINYINT(1) NOT NULL DEFAULT 0, ADD budget TINYINT(1) NOT NULL DEFAULT 0');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE band_final_score DROP opendoor, DROP aftershow, DROP budget');
    }
}
